import 'package:get/get.dart';

import 'auth_api_interceptor.dart';

class AuthApiClient {
  final GetHttpClient client = GetHttpClient();

  AuthApiClient({
    AuthApiInterceptor authApiInterceptor,
  }) {
    // client.baseUrl = Get.find<Env>().kAuthBaseUrl;
    client.timeout = Duration(seconds: 15);
    client.addRequestModifier(
        (request) => authApiInterceptor.requestModifier(request));
    client.addResponseModifier((request, response) =>
        authApiInterceptor.responseModifier(request, response));
  }
}
