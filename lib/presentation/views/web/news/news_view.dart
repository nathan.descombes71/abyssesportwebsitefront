import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/scaffold/loading_scaffold.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/news/news_content/news_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'news_view_controller.dart';

class NewsView extends GetView<NewsViewController> {
  NewsView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onLoading: LoadingScaffold(),
    );
  }

  ScrollController scrollController = ScrollController();

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: Container(
        decoration: BoxDecoration(
          // color: Color(0xFFF262626),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/graph/backgroundtest.png'),
          ),
        ),
        child: Center(
          child: LayoutBuilder(
            builder: (contextLayout, constraint) => ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: contextLayout.height - 105,
              ),
              child: Container(
                margin: EdgeInsets.only(
                    left: contextLayout.width > 800
                        ? contextLayout.width * 0.15
                        : 0,
                    right: contextLayout.width > 800
                        ? contextLayout.width * 0.15
                        : 0),
                padding: EdgeInsets.fromLTRB(100, 60, 100, 60),
                // color: Color(0xFFF262626),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 60,
                          width: 5,
                          color: Colors.white.withOpacity(0.3),
                        ),
                        SizedBox(width: 20),
                        Text(
                          "currentNews".tr,
                          style: kTitleStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    controller.allNews.isNotEmpty
                        ? Padding(
                            padding: EdgeInsets.only(top: 40),
                            child: Container(
                                height: contextLayout.width > 700
                                    ? (200 * controller.allNews.length)
                                            .toDouble() +
                                        5.0
                                    : (281 * controller.allNews.length)
                                        .toDouble(),
                                width: 800,

                                // height: 800,
                                child: Wrap(
                                  children: [
                                    for (var i = 0;
                                        i < controller.allNews.length;
                                        i++)
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 5, left: 15, right: 15),
                                        child: InkWell(
                                            onTap: () {
                                              // GetPage(
                                              //   name: '/test',
                                              //   page: () => DetailsNiewsView(),
                                              //   binding: DetailsNiewsViewControllerBindings(),
                                              // );
                                              Get.offAndToNamed(
                                                Routes.NEWS +
                                                    '/' +
                                                    controller.allNews[i].id
                                                        .toString(),
                                              );
                                            },
                                            child: NewsCard(
                                                actualNews:
                                                    controller.allNews[i])),
                                      ),
                                  ],
                                )

                                // ListView.builder(
                                //   itemCount: controller.allNews.length,
                                //   itemBuilder: (contextLayout, i) {
                                //     return Padding(
                                //       padding: const EdgeInsets.only(
                                //           top: 5, left: 7, right: 7),
                                //       child: InkWell(
                                //           onTap: () {
                                //             // GetPage(
                                //             //   name: '/test',
                                //             //   page: () => DetailsNiewsView(),
                                //             //   binding: DetailsNiewsViewControllerBindings(),
                                //             // );
                                //             Get.offAndToNamed(
                                //               Routes.NEWS +
                                //                   '/' +
                                //                   controller.allNews[i].id
                                //                       .toString(),
                                //             );
                                //           },
                                //           child: NewsCard(
                                //               actualNews: controller.allNews[i])),
                                //     );
                                //   },
                                // ),
                                ),
                          )
                        : Center(child: Text('noNews'.tr)),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
