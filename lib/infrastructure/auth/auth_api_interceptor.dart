
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:get/get_connect/http/src/response/response.dart';

import '../api/rest_api_logger.dart';

class AuthApiInterceptor extends RestApiLogger {
  Request requestModifier(Request request) {
    onRequestLogger(request);
    // TODO Edit request here //

    // ---------------------- //
    return request;
  }

  Response responseModifier(Request request, Response response) {
    onResponseLogger(response);
    // TODO Edit request here //

    // ---------------------- //
    return response;
  }
}
