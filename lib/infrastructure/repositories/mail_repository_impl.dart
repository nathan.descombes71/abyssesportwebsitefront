import 'package:abyssesportwebsite/domain/feature/mail/repositories/mail_repositories.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/repositories/sponsor_repositories.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/sponsor_dto/sponsor_dto.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/rest_api_repository_factory.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'dart:convert' as convert;

import '../api/rest_api_client.dart';

class MailRepositoryImpl extends RestApiRepositoryFactory
    implements MailRepository {
  MailRepositoryImpl({
    GetHttpClient client,
  }) : super(
          controller: '/sending',
          client: client ?? Get.find<RestApiClient>().client,
        );

  @override
  Future<String> postMail(
      {String mail,
      String firstName,
      String lastName,
      String pseudo,
      String subject,
      String message}) async {
    try {
      var response = await client.post('$controller',
          body:
              '{"mail":"$mail", "subject":"$subject", "firstname":"$firstName", "lastname":"$lastName", "pseudo":"$pseudo", "message":"$message"}');
      return response.body;
    } catch (e) {
      throw (e);
    }
  }
}
