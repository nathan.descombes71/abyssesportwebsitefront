import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sponsor_dto.freezed.dart';
part 'sponsor_dto.g.dart';

@freezed
abstract class SponsorDto with _$SponsorDto {
  const factory SponsorDto({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name_sponsor') String nameSponsor,
    @JsonKey(name: 'link_redirect') String linkRedirect,
    @JsonKey(name: 'path_image') String pathImage,
  }) = _SponsorDto;
  factory SponsorDto.fromJson(Map<String, dynamic> json) =>
      _$SponsorDtoFromJson(json);
}

extension OnSponsorJson on Map<String, dynamic> {
  SponsorDto get toSponsorDto {
    return SponsorDto.fromJson(this);
  }
}

extension OnListSponsorJson on List<Map<String, dynamic>> {
  List<SponsorDto> get toSponsorDto {
    return map<SponsorDto>((Map<String, dynamic> e) => e.toSponsorDto).toList();
  }
}
