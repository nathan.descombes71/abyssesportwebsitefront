import 'package:abyssesportwebsite/infrastructure/dtos/player_dto/player_dto.dart';

class Players {
  int id;
  String pseudo;
  String country;
  int age;
  String role;
  String jeu;
  String cardPath;
  bool isStaff;
  String roleStaff;

  Players({
    this.id,
    this.age,
    this.country,
    this.pseudo,
    this.role,
    this.jeu,
    this.cardPath,
    this.isStaff,
    this.roleStaff,
  });
}

extension OnPlayers on Players {
  Players copyWith({
    int id,
    String pseudo,
    String country,
    int age,
    String role,
    String jeu,
    String cardPath,
    bool isStaff,
    String roleStaff,
  }) {
    return Players(
      id: id ?? this.id,
      pseudo: pseudo ?? this.pseudo,
      country: country ?? this.country,
      age: age ?? this.age,
      role: role ?? this.role,
      jeu: jeu ?? this.jeu,
      cardPath: cardPath ?? this.cardPath,
      isStaff: isStaff ?? this.isStaff,
      roleStaff: roleStaff ?? this.roleStaff,
    );
  }
     PlayerDto get toDto {
    return PlayerDto(
      id: id,
      age: age,
      cardPath: cardPath,
      country: country,
      isStaff: isStaff,
      jeu: jeu,
      pseudo: pseudo,
      role: role,
      roleStaff: roleStaff,   
      );
  }
}

extension OnListPlayers on List<Players> {
  List<PlayerDto> get toDto {
    List<PlayerDto> _playersDtoList = [];

    this?.forEach((entity) => _playersDtoList.add(entity.toDto));
    return _playersDtoList;
  }
}

extension OnListPlayersDto on List<PlayerDto> {
  List<Players> get toEntity {
    List<Players> _playersList = [];

    this?.forEach((dto) => _playersList.add(dto.toEntity));
    return _playersList;
  }
}

extension OnPlayersDto on PlayerDto {
  Players get toEntity {
    return Players(
      id: id,
      age: age,
      cardPath: cardPath,
      country: country,
      isStaff: isStaff,
      jeu: jeu,
      pseudo: pseudo,
      role: role,
      roleStaff: roleStaff,
    );
  }
}