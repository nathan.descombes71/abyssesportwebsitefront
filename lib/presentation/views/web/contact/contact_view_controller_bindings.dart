import 'package:abyssesportwebsite/domain/feature/mail/controller/mail_controller.dart';
import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'contact_view_controller.dart';

class ContactViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ContactViewController(Get.put<MailController>(MailController())),
    );
  }
}
