import 'package:abyssesportwebsite/domain/feature/teams/entities/teams.dart';
import 'package:abyssesportwebsite/presentation/core/widgets/x_gradient_divider.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_content/manager_player_card.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../../../core/styles/styles.dart';

class TeamsChange extends GetView<ManagerViewController> {
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: controller.scrollbar,
      child: SingleChildScrollView(
        controller: controller.scrollbar,
        child: Obx(
          () => Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Row(
                children: [
                  Container(
                    height: 60,
                    width: 5,
                    color: Colors.white.withOpacity(0.3),
                  ),
                  SizedBox(width: 20),
                  SelectableText(
                    "EQUIPE ACTUELLE",
                    style: kTitleStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              for (var i = 0; i < controller.teamsList.length; i++)
                Column(
                  children: [
                    Container(
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Obx(() => Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          Get.dialog(dialogDeleteTeams(
                                              controller.teamsList[i]));
                                        },
                                        icon: Icon(
                                          MdiIcons.delete,
                                          color: Colors.white,
                                          size: 25,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      SelectableText(
                                        controller.teamsList[i].nameTeams + ':',
                                        style: kTitleStyle,
                                      ),
                                    ],
                                  )),
                            ),
                            Center(
                              child: Wrap(
                                children: [
                                  for (var index = 0;
                                      index <
                                          controller
                                              .teamsList[i].listPlayers.length;
                                      index++)
                                    Padding(
                                      padding: EdgeInsets.all(30),
                                      child: ManagerPlayerCard(
                                        controller: controller,
                                        actualPlayer: controller
                                            .teamsList[i].listPlayers[index],
                                        canSeePlayers: true.obs,
                                        index: i,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        controller.createPlayer(i);
                      },
                      child: Center(
                        child: Icon(
                          MdiIcons.plusCircleOutline,
                          color: Colors.white,
                          size: 60,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text('Logo actuelle:'),
                    SizedBox(
                      height: 10,
                    ),
                    controller.teamsList[i].pathLogoTeam != null
                        ? Container(
                            decoration: BoxDecoration(
                                color: Color(0xFF7a3f8a),
                                border:
                                    Border.all(color: Colors.black, width: 7)),
                            padding: EdgeInsets.all(10),
                            height: 100,
                            width: 100,
                            child: Obx(
                              () => Image.network(
                                controller.teamsList[i].pathLogoTeam,
                                color: Colors.black,
                              ),
                            ),
                          )
                        : Text('Aucun logo pour le moment'),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child: Obx(
                      () => controller.listAllImagePicked
                                  .lastWhere((element) =>
                                      element.value.idTeams ==
                                      controller.teamsList[i].id)
                                  .value
                                  .linkImage ==
                              null
                          ? Text(
                              'veuillez séléctionner un logo pour la preview')
                          : Container(
                              decoration: BoxDecoration(
                                  color: Color(0xFF7a3f8a),
                                  border: Border.all(
                                      color: Colors.black, width: 7)),
                              padding: EdgeInsets.all(10),
                              height: 100,
                              width: 100,
                              child: Obx(
                                () => Image.network(
                                  controller.listAllImagePicked
                                      .lastWhere((element) =>
                                          element.value.idTeams ==
                                          controller.teamsList[i].id)
                                      .value
                                      .linkImage
                                      .path,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                    )),
                    Obx(() => controller.listAllImagePicked
                                .lastWhere((element) =>
                                    element.value.idTeams ==
                                    controller.teamsList[i].id)
                                .value
                                .linkImage !=
                            null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                  icon: Icon(
                                    MdiIcons.check,
                                    color: Colors.green,
                                  ),
                                  onPressed: () {
                                    controller
                                        .sendImage(controller.teamsList[i].id);
                                  }),
                              IconButton(
                                  icon: Icon(
                                    MdiIcons.close,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    controller.listAllImagePicked
                                        .lastWhere((element) =>
                                            element.value.idTeams ==
                                            controller.teamsList[i].id)
                                        .value
                                        .linkImage = null;
                                    controller.refresh();
                                  }),
                            ],
                          )
                        : SizedBox()),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child: ElevatedButton(
                      onPressed: () {
                        controller.getImage(controller.teamsList[i].id);
                      },
                      child: Text('Choisir un logo'),
                    )),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                        child: ElevatedButton(
                      onPressed: () {
                        Get.dialog(palmaDialog(i));
                      },
                      child: Text('Ajouter un titre au palmares'),
                    )),
                    SizedBox(
                      height: 30,
                    ),
                    XGradientDivider.white(),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              controller.teamsList.length == 0
                  ? Text('Aucune équipe Abyss eSport')
                  : SizedBox(),
              Center(
                  child: ElevatedButton(
                onPressed: () {
                  Get.dialog(dialogCreate());
                },
                child: Text('Ajouter une équipe'),
              )),
              SizedBox(
                height: 200,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dialogDeleteTeams(Teams teams) {
    return Dialog(
        child: IntrinsicHeight(
      child: IntrinsicWidth(
        child: Container(
          color: Color(0xFFF262626),
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Text('Etes vous certains de vouloir supprimer ' +
                    teams.nameTeams +
                    '. vous perdrez toutes vos données.'),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Color(0xFF8338a1),
                        ),
                      ),
                      onPressed: () {
                        controller.deleteTeams(teams.id);
                        Get.back();
                      },
                      child: Text('Supprimer'),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('Annuler'),
                    ),
                  ],
                ),
              ],
            ),
          )),
        ),
      ),
    ));
  }

  Widget dialogCreate() {
    return Dialog(
        child: IntrinsicHeight(
      child: IntrinsicWidth(
        child: Container(
          color: Color(0xFFF262626),
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                TextFormField(
                  onChanged: (value) {
                    controller.nameTeams = value;
                  },
                  decoration: InputDecoration(
                      fillColor: Colors.grey.withOpacity(0.6),
                      focusColor: Colors.grey.withOpacity(0.6),
                      hoverColor: Colors.grey.withOpacity(0.6),
                      border: InputBorder.none,
                      hintText: 'Entrez un nom d\'équipe'),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  onChanged: (value) {
                    controller.nameGames = value;
                  },
                  decoration: InputDecoration(
                      fillColor: Colors.grey.withOpacity(0.6),
                      focusColor: Colors.grey.withOpacity(0.6),
                      hoverColor: Colors.grey.withOpacity(0.6),
                      border: InputBorder.none,
                      hintText: 'Entrez le nom du jeu'),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {
                    controller.createTeam();
                    controller.refresh();
                  },
                  child: Text('Créer cette équipe'),
                ),
              ],
            ),
          )),
        ),
      ),
    ));
  }

  Widget palmaDialog(int indexTeams) {
    return Obx(() =>
        controller.constructDialog.value || !controller.constructDialog.value
            ? Dialog(
                child: IntrinsicHeight(
                  child: Container(
                    height: 400,
                    width: 400,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: 8.0, bottom: 8, left: 10, right: 10),
                          child: Container(
                            height: 200,
                            child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: controller
                                    .teamsList[indexTeams].listPalmares.length,
                                itemBuilder: (buildContext, index) {
                                  return Card(
                                    color: Colors.transparent,
                                    shadowColor: Colors.transparent,
                                    child: Container(
                                      height: 30,
                                      child: Row(
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                controller
                                                    .teamsList[indexTeams]
                                                    .listPalmares[index]
                                                    .positionImage,
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Text(controller
                                                    .teamsList[indexTeams]
                                                    .listPalmares[index]
                                                    .nameCup),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: SizedBox(),
                                          ),
                                          IconButton(
                                              onPressed: () {
                                                controller.deletePalmares(
                                                    controller
                                                        .teamsList[indexTeams]
                                                        .listPalmares[index]
                                                        .id,
                                                    indexTeams);
                                              },
                                              icon: Icon(
                                                MdiIcons.delete,
                                                color: Colors.red,
                                              ))
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            onChanged: (value) {
                              controller.posTeam = value;
                            },
                            decoration: InputDecoration(
                              labelText: "Position de la team",
                              // fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            // initialValue: 'test',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            onChanged: (value) {
                              controller.nameCup = value;
                            },
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              labelText: "Nom de la coupe",
                              // fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            // initialValue: 'test',
                          ),
                        ),
                        IconButton(
                            onPressed: () {
                              controller.addPalmares(indexTeams);
                            },
                            icon: Icon(
                              MdiIcons.plus,
                              color: Colors.green,
                              size: 25,
                            ))
                      ],
                    ),
                  ),
                ),
              )
            : SizedBox());
  }
}
