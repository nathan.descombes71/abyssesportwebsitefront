Map<String, String> get frFR {
  return const {
    //ACCUEIL
    'home': 'ACCUEIL',
    'lastNews': 'DERNIERES NEWS',
    'about': 'A PROPOS',
    'thank': 'Merci a vous !',
    'allRightsReserved': 'tous droits réservés',
    'aboutusText':
        'La Abyss eSport est une structure eSportive qui a pour ambition de devenir l’un des leaders français. Présente sur two jeux, Valorant et Rocket League, la Abyss eSport veut performer au maximum et former les meilleurs joueurs mondiaux!',
    'anySponsor': 'Aucun sponsor actuellement',
    //CONTACT
    'lastName': 'Nom',
    'firstName': 'Prénom',
    'object': 'Objet',
    'mail': 'Adresse mail',
    'yourMessage': 'Votre message',
    'send': 'Envoyer',
    'emailSentSuccess': 'Email envoyé avec succès !',
    'becameSponsor': 'Devenir un sponsor',
    //NEWS
    'currentNews': 'NEWS ACTUELLES',
    'noNews': 'Aucune news disponible pour le moment',
    'writtenBy': 'Rédigé par',
    'on': 'le',
    //Teams
    'seePlayersInfo': 'Voir les informations des joueurs',
    'awards': 'PALMARES',
    'player': 'Joueur',
    'players': 'PLAYERS',
    'captain': 'Capitaine',
    //Error
    'objectFieldError': 'veuillez remplir le champ objet',
    'emailFieldError': 'email non valide',
    'messageFieldError': 'veuillez remplir le champ Message',
  };
}
