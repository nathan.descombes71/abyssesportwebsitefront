import 'package:abyssesportwebsite/domain/feature/mail/controller/mail_controller.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContactViewController extends GetxController with StateMixin {
  final MailController mailController;
  ContactViewController(this.mailController);

  final formKey = GlobalKey<FormState>();

  String objet = '';
  String nom = '';
  String prenom = '';
  String pseudo = '';
  String messageMail = '';
  String email = '';
  RxString error = ''.obs;
  bool isSponsoring = false;

  TextEditingController controllerObjectText = TextEditingController(text: '');

  @override
  void onInit() {
    change(
      null,
      status: RxStatus.loading(),
    );

    (Get.arguments as Map<String, dynamic>)?.forEach(
      (key, value) {
        if (key == 'isSponsoring') {
          isSponsoring = true;
        }
      },
    );

    if (isSponsoring) {
      controllerObjectText.text = 'becameSponsor'.tr;
    }
    print(isSponsoring);

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void sendEmail() async {
    var response = await mailController.postMail(
        firstName: prenom.replaceAll('"', "'"),
        lastName: nom.replaceAll('"', "'"),
        mail: email,
        message: messageMail.replaceAll('"', "'"),
        pseudo: pseudo.replaceAll('"', "'"),
        subject: objet.replaceAll('"', "'"));

    print(response);

    succesSnackBar(title: '', text: response.tr);
  }
}
