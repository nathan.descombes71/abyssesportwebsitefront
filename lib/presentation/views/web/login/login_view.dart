import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login_view_controller.dart';

class LoginView extends GetView<LoginViewController> {
  LoginView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: LayoutBuilder(
        builder: (contextLayout, constraints) => ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: contextLayout.height - 105,
          ),
          child: Container(
            margin: EdgeInsets.only(
                left:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0,
                right:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0),
            // height: contextLayout.height - 105,
            color: Color(0xFFF262626),
            child: Center(
              child: Container(
                // height: 300,
                width: 400,
                child: Obx(
                  () => Column(
                    children: [
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 60,
                            width: 5,
                            color: Colors.white.withOpacity(0.3),
                          ),
                          SizedBox(width: 20),
                          SelectableText(
                            'SE CONNECTER',
                            style: kTitleStyle,
                          ),
                        ],
                      ),
                      SizedBox(height: 50),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        onChanged: (value) {
                          if (value != null) {
                            controller.email = value;
                          }
                        },
                        decoration: InputDecoration(
                          labelText: "Adresse Mail",
                          // fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        // initialValue: 'test',
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        onChanged: (value) {
                          if (value != null) {
                            controller.pass = value;
                          }
                        },
                        decoration: InputDecoration(
                          labelText: "Mot de passe",
                          // fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        // initialValue: 'test',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      if (controller.errorMessage.value.isNotEmpty)
                        Obx(
                          () => Text(controller.errorMessage.value,
                              style: kErrorTextStyle),
                        ),
                      SizedBox(
                        height: 15,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Color(0xFF7a3f8a),
                          onSurface: Colors.grey,
                        ),
                        onPressed: () async {
                          controller.errorMessage.value =
                              await controller.signIn();

                          print(controller.errorMessage.value);
                        },
                        child: Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            'Se connecter',
                            style: kTextStyle.copyWith(fontSize: 17),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
