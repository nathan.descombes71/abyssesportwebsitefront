import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
import 'package:abyssesportwebsite/presentation/core/helpers/data_converter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsCard extends StatelessWidget {
  final News actualNews;

  const NewsCard({Key key, this.actualNews}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 425,
      width: 300,
      child: Stack(
        children: [
          Container(
            height: 300,
            width: 300,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                      actualNews.linkImage,
                    ),
                    fit: BoxFit.cover)),
            // child: Image.network(actualNews.linkImage, ),
          ),
          Positioned(
              top: 200,
              height: 150,
              width: 300,
              child: Container(
                padding: EdgeInsets.only(top: 20, left: 10),
                color: Color(0xFFF262626),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      actualNews.title,
                    ),
                    SizedBox(height: 10),
                    Text('writtenBy'.tr + ' ' + actualNews.author),
                    SizedBox(height: 10),
                    Text('on'.tr + ' ' + actualNews.createdAt),
                  ],
                ),
              ))
        ],
      ),
    );
    // return Card(
    //   color: Color(0xFFF262626),
    //   child: LayoutBuilder(
    //     builder: (contextCard, constraint) => Container(
    //       // height: 150,
    //       child: Padding(
    //         padding: const EdgeInsets.all(8.0),
    //         child: Get.width > 700
    //             ? Row(
    //                 children: [
    //                   Container(
    //                       height: 140,
    //                       width: 140,
    //                       child: Image.network(actualNews.linkImage)),
    //                   Container(
    //                     margin: EdgeInsets.fromLTRB(10, 10, 15, 10),
    //                     width: 2,
    //                     height: 150,
    //                     color: Colors.white.withOpacity(0.5),
    //                   ),
    //                   Expanded(
    //                     child: Column(
    //                       crossAxisAlignment: CrossAxisAlignment.start,
    //                       mainAxisAlignment: MainAxisAlignment.center,
    //                       children: [
    //                         Text(
    //                           actualNews.title,
    //                         ),
    //                         SizedBox(height: 10),
    //                         Text('writtenBy'.tr + ' ' + actualNews.author),
    //                         SizedBox(height: 10),
    //                         Text('on'.tr + ' ' + actualNews.createdAt),
    //                       ],
    //                     ),
    //                   )
    //                 ],
    //               )
    //             : Center(
    //                 child: Stack(
    //                   children: [
    //                     Container(
    //                         height: 250,
    //                         width: 250,
    //                         child: Image.network(actualNews.linkImage)),
    //                     Padding(
    //                       padding: const EdgeInsets.all(18.0),
    //                       child: Container(
    //                           height: 200,
    //                           width: 230,
    //                           child:
    //                               Column(children: [Text(actualNews.title)])),
    //                     ),
    //                     Positioned(
    //                         left: 18,
    //                         bottom: 18,
    //                         child:
    //                             Text('writtenBy'.tr + ' ' + actualNews.author))
    //                   ],
    //                 ),
    //               ),
    //       ),
    //     ),
    //   ),
    // );
  }
}
