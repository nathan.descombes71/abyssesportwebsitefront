import 'package:abyssesportwebsite/domain/feature/news/repositories/news_repositories.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/news_dto/news_dto.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/rest_api_repository_factory.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'dart:convert' as convert;

import '../api/rest_api_client.dart';

class NewsRepositoryImpl extends RestApiRepositoryFactory<NewsDto>
    implements NewsRepository {
  NewsRepositoryImpl({
    GetHttpClient client,
  }) : super(
          controller: '/news',
          client: client ?? Get.find<RestApiClient>().client,
        );

  @override
  Future<List<NewsDto>> getNewsDtoList(
      {Map<String, dynamic> queryParameters}) async {
    try {
      List<NewsDto> newsList = [];

      var response = await client.get('$controller');

      print('1');

      if (response.body != null) {
        var jsonResponse = convert.jsonDecode(response.body);
        print(convert.jsonDecode(response.body));
        newsList = jsonResponse['data'].map<NewsDto>((e) {
          return NewsDto.fromJson(e);
        }).toList();
      }
      return newsList;
    } catch (e) {
      print(e.toString());
      throw (e);
      // errorSnackBar(text: e.toString());
    }
  }

  @override
  Future<String> createNew(NewsDto news) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);

      var response = await client
          .post('$controller/create', body: news.toJson(), headers: {
        "Authorization": "Bearer " + token,
      });

      print(news.toJson());

      return response.body;
    } catch (e) {
      throw (e);
    }
  }

  @override
  Future<String> deleteNews(int idNews) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);

      var response = await client.delete('$controller/$idNews', headers: {
        "Authorization": "Bearer " + token,
      });

      return response.body;
    } catch (e) {
      throw (e);
    }
  }
}
