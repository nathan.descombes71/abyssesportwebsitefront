import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/controller/sponsor_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'accueil_view_controller.dart';

class AccueilViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => AccueilViewController(
        newsController: Get.put<NewsController>(
          NewsController(),
        ),
        sponsorController: Get.put<SponsorController>(
          SponsorController(),
        ),
      ),
    );
  }
}
