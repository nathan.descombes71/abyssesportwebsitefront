// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'team_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
TeamDto _$TeamDtoFromJson(Map<String, dynamic> json) {
  return _TeamDto.fromJson(json);
}

/// @nodoc
class _$TeamDtoTearOff {
  const _$TeamDtoTearOff();

// ignore: unused_element
  _TeamDto call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'name_teams') String nameTeams,
      @JsonKey(name: 'path_logo_team') String pathLogoTeams,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'list_players') List<PlayerDto> listPlayers,
      @JsonKey(name: 'palmares') List<PalmaresDto> listPalmares}) {
    return _TeamDto(
      id: id,
      nameTeams: nameTeams,
      pathLogoTeams: pathLogoTeams,
      jeu: jeu,
      listPlayers: listPlayers,
      listPalmares: listPalmares,
    );
  }

// ignore: unused_element
  TeamDto fromJson(Map<String, Object> json) {
    return TeamDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $TeamDto = _$TeamDtoTearOff();

/// @nodoc
mixin _$TeamDto {
  @JsonKey(name: 'id')
  String get id;
  @JsonKey(name: 'name_teams')
  String get nameTeams;
  @JsonKey(name: 'path_logo_team')
  String get pathLogoTeams;
  @JsonKey(name: 'jeu')
  String get jeu;
  @JsonKey(name: 'list_players')
  List<PlayerDto> get listPlayers;
  @JsonKey(name: 'palmares')
  List<PalmaresDto> get listPalmares;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $TeamDtoCopyWith<TeamDto> get copyWith;
}

/// @nodoc
abstract class $TeamDtoCopyWith<$Res> {
  factory $TeamDtoCopyWith(TeamDto value, $Res Function(TeamDto) then) =
      _$TeamDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'name_teams') String nameTeams,
      @JsonKey(name: 'path_logo_team') String pathLogoTeams,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'list_players') List<PlayerDto> listPlayers,
      @JsonKey(name: 'palmares') List<PalmaresDto> listPalmares});
}

/// @nodoc
class _$TeamDtoCopyWithImpl<$Res> implements $TeamDtoCopyWith<$Res> {
  _$TeamDtoCopyWithImpl(this._value, this._then);

  final TeamDto _value;
  // ignore: unused_field
  final $Res Function(TeamDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object nameTeams = freezed,
    Object pathLogoTeams = freezed,
    Object jeu = freezed,
    Object listPlayers = freezed,
    Object listPalmares = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      nameTeams: nameTeams == freezed ? _value.nameTeams : nameTeams as String,
      pathLogoTeams: pathLogoTeams == freezed
          ? _value.pathLogoTeams
          : pathLogoTeams as String,
      jeu: jeu == freezed ? _value.jeu : jeu as String,
      listPlayers: listPlayers == freezed
          ? _value.listPlayers
          : listPlayers as List<PlayerDto>,
      listPalmares: listPalmares == freezed
          ? _value.listPalmares
          : listPalmares as List<PalmaresDto>,
    ));
  }
}

/// @nodoc
abstract class _$TeamDtoCopyWith<$Res> implements $TeamDtoCopyWith<$Res> {
  factory _$TeamDtoCopyWith(_TeamDto value, $Res Function(_TeamDto) then) =
      __$TeamDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'name_teams') String nameTeams,
      @JsonKey(name: 'path_logo_team') String pathLogoTeams,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'list_players') List<PlayerDto> listPlayers,
      @JsonKey(name: 'palmares') List<PalmaresDto> listPalmares});
}

/// @nodoc
class __$TeamDtoCopyWithImpl<$Res> extends _$TeamDtoCopyWithImpl<$Res>
    implements _$TeamDtoCopyWith<$Res> {
  __$TeamDtoCopyWithImpl(_TeamDto _value, $Res Function(_TeamDto) _then)
      : super(_value, (v) => _then(v as _TeamDto));

  @override
  _TeamDto get _value => super._value as _TeamDto;

  @override
  $Res call({
    Object id = freezed,
    Object nameTeams = freezed,
    Object pathLogoTeams = freezed,
    Object jeu = freezed,
    Object listPlayers = freezed,
    Object listPalmares = freezed,
  }) {
    return _then(_TeamDto(
      id: id == freezed ? _value.id : id as String,
      nameTeams: nameTeams == freezed ? _value.nameTeams : nameTeams as String,
      pathLogoTeams: pathLogoTeams == freezed
          ? _value.pathLogoTeams
          : pathLogoTeams as String,
      jeu: jeu == freezed ? _value.jeu : jeu as String,
      listPlayers: listPlayers == freezed
          ? _value.listPlayers
          : listPlayers as List<PlayerDto>,
      listPalmares: listPalmares == freezed
          ? _value.listPalmares
          : listPalmares as List<PalmaresDto>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_TeamDto implements _TeamDto {
  const _$_TeamDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'name_teams') this.nameTeams,
      @JsonKey(name: 'path_logo_team') this.pathLogoTeams,
      @JsonKey(name: 'jeu') this.jeu,
      @JsonKey(name: 'list_players') this.listPlayers,
      @JsonKey(name: 'palmares') this.listPalmares});

  factory _$_TeamDto.fromJson(Map<String, dynamic> json) =>
      _$_$_TeamDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String id;
  @override
  @JsonKey(name: 'name_teams')
  final String nameTeams;
  @override
  @JsonKey(name: 'path_logo_team')
  final String pathLogoTeams;
  @override
  @JsonKey(name: 'jeu')
  final String jeu;
  @override
  @JsonKey(name: 'list_players')
  final List<PlayerDto> listPlayers;
  @override
  @JsonKey(name: 'palmares')
  final List<PalmaresDto> listPalmares;

  @override
  String toString() {
    return 'TeamDto(id: $id, nameTeams: $nameTeams, pathLogoTeams: $pathLogoTeams, jeu: $jeu, listPlayers: $listPlayers, listPalmares: $listPalmares)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TeamDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.nameTeams, nameTeams) ||
                const DeepCollectionEquality()
                    .equals(other.nameTeams, nameTeams)) &&
            (identical(other.pathLogoTeams, pathLogoTeams) ||
                const DeepCollectionEquality()
                    .equals(other.pathLogoTeams, pathLogoTeams)) &&
            (identical(other.jeu, jeu) ||
                const DeepCollectionEquality().equals(other.jeu, jeu)) &&
            (identical(other.listPlayers, listPlayers) ||
                const DeepCollectionEquality()
                    .equals(other.listPlayers, listPlayers)) &&
            (identical(other.listPalmares, listPalmares) ||
                const DeepCollectionEquality()
                    .equals(other.listPalmares, listPalmares)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(nameTeams) ^
      const DeepCollectionEquality().hash(pathLogoTeams) ^
      const DeepCollectionEquality().hash(jeu) ^
      const DeepCollectionEquality().hash(listPlayers) ^
      const DeepCollectionEquality().hash(listPalmares);

  @JsonKey(ignore: true)
  @override
  _$TeamDtoCopyWith<_TeamDto> get copyWith =>
      __$TeamDtoCopyWithImpl<_TeamDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_TeamDtoToJson(this);
  }
}

abstract class _TeamDto implements TeamDto {
  const factory _TeamDto(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'name_teams') String nameTeams,
      @JsonKey(name: 'path_logo_team') String pathLogoTeams,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'list_players') List<PlayerDto> listPlayers,
      @JsonKey(name: 'palmares') List<PalmaresDto> listPalmares}) = _$_TeamDto;

  factory _TeamDto.fromJson(Map<String, dynamic> json) = _$_TeamDto.fromJson;

  @override
  @JsonKey(name: 'id')
  String get id;
  @override
  @JsonKey(name: 'name_teams')
  String get nameTeams;
  @override
  @JsonKey(name: 'path_logo_team')
  String get pathLogoTeams;
  @override
  @JsonKey(name: 'jeu')
  String get jeu;
  @override
  @JsonKey(name: 'list_players')
  List<PlayerDto> get listPlayers;
  @override
  @JsonKey(name: 'palmares')
  List<PalmaresDto> get listPalmares;
  @override
  @JsonKey(ignore: true)
  _$TeamDtoCopyWith<_TeamDto> get copyWith;
}
