import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'news_view_controller.dart';

class NewsViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => NewsViewController(
        newsController: Get.put<NewsController>(
          NewsController(),
        ),
      ),
    );
  }
}
