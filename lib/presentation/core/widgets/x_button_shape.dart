import 'package:flutter/material.dart';

class XButtonShape implements MaterialStateProperty<OutlinedBorder> {
  final OutlinedBorder shape;
  XButtonShape._({
    @required this.shape,
  });
  factory XButtonShape({
    OutlinedBorder shape,
  }) {
    return XButtonShape._(
      shape: shape ??
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(3.0),
          ),
    );
  }
  @override
  OutlinedBorder resolve(Set<MaterialState> states) {
    // Always return the same border no matter what is the state
    return shape;
  }
}
