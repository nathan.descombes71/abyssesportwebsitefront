import 'package:abyssesportwebsite/domain/feature/sponsor/entities/sponsor.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CreateSponsor extends GetView<ManagerViewController> {
  CreateSponsor({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildContent(context);
  }

  Widget buildContent(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            children: [
              Container(
                height: 60,
                width: 5,
                color: Colors.white.withOpacity(0.3),
              ),
              SizedBox(width: 20),
              SelectableText(
                "CREER UN SPONSOR",
                style: kTitleStyle,
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            width: 300,
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              // obscureText: true,
              onChanged: (value) {
                controller.nameSponsor = value;
              },
              decoration: InputDecoration(
                labelText: "Nom du sponsor",
                // fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                  borderSide: new BorderSide(),
                ),
              ),
              // initialValue: 'test',
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              // obscureText: true,
              onChanged: (value) {
                controller.linkRedirectSponsor = value;
              },
              decoration: InputDecoration(
                labelText: "Lien de redirection",
                // fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                  borderSide: new BorderSide(),
                ),
              ),

              // initialValue: 'test',
            ),
          ),
          SizedBox(
            height: 30,
          ),
          controller.logoSponsor.value.path == ''
              ? SizedBox()
              : Image.network(
                  controller.logoSponsor.value.path,
                  width: 300,
                  height: 300,
                ),
          SizedBox(
            height: 30,
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(
                Color(0xFF8338a1),
              ),
            ),
            onPressed: () {
              controller.getLogoSponsor();
            },
            child: Text('Ajouter un logo'),
          ),
          SizedBox(
            height: 30,
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(
                Color(0xFF8338a1),
              ),
            ),
            onPressed: () {
              controller.createSponsor();
            },
            child: Text('Créer ce sponsor'),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            // height: 500,
            child: Center(
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 50, 0, 50),
                child: LayoutBuilder(
                    builder: (contextLayout, constraints) => Wrap(
                          alignment: WrapAlignment.start,
                          children: [
                            for (var i = 0;
                                i < controller.sponsorList.length;
                                i++)
                              sponsorLogo(
                                  actualSponsor: controller.sponsorList[i]),
                          ],
                        )),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget sponsorLogo({
    Sponsor actualSponsor,
  }) {
    RxDouble sizeLogoSponsor = 150.0.obs;
    return Container(
      width: 300,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(actualSponsor.nameSponsor, style: kTextStyle),
              IconButton(
                onPressed: () {
                  controller.deleteSponsor(actualSponsor.id);
                },
                icon: Icon(
                  MdiIcons.delete,
                  color: Colors.red,
                ),
              )
            ],
          ),
          Container(
            height: 200,
            width: 200,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Center(
              child: InkWell(
                onTap: () {},
                onHover: (isHover) {
                  if (isHover) {
                    sizeLogoSponsor.value = 180;
                  } else {
                    sizeLogoSponsor.value = 150;
                  }
                  print(sizeLogoSponsor.value);
                },
                child: Obx(
                  () => AnimatedContainer(
                    height: sizeLogoSponsor.value,
                    width: sizeLogoSponsor.value,
                    duration: Duration(milliseconds: 200),
                    child: Image.network(
                      actualSponsor.pathImage,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
