import 'package:abyssesportwebsite/domain/auth/auth_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'create_article_view_controller.dart';

class CreateArticleViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => CreateArticleViewController(
       
      ),
    );
  }
}
