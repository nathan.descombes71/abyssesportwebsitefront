import 'package:abyssesportwebsite/infrastructure/api/rest_api_client.dart';
import 'package:abyssesportwebsite/infrastructure/api/rest_api_interceptor.dart';
import 'package:abyssesportwebsite/presentation/navigation/navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:url_strategy/url_strategy.dart';

import 'domain/feature/user/entities/user.dart';
import 'presentation/core/styles/theme_datas.dart';
import 'presentation/core/translations/localization_service.dart';
import 'presentation/navigation/routes.dart';

// import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  String initialRoute = Routes.ACCUEIL;
  // setPathUrlStrategy();
  runApp(
    GetMaterialApp(
      title: 'Abyss eSport',
      onGenerateRoute: Nav.routesGenerate,
      initialRoute: initialRoute,
      // getPages: Nav.routes,
      theme: XThemeData.light(),
      darkTheme: XThemeData.dark(),
      themeMode: ThemeMode.dark,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: getLocale(Get.deviceLocale),
      supportedLocales: const <Locale>[
        Locale('fr', 'FR'),
        Locale('en', 'US'),
      ],
      fallbackLocale: Locale('fr', 'FR'),
      translations: LocalizationService(),
    ),
  );
}

Locale getLocale(Locale locale) {
  if (locale.languageCode != null && locale.countryCode == null) {
    switch (locale.languageCode) {
      case 'fr':
        return Locale('fr', 'FR');
        break;
      case 'de':
        return Locale('de', 'DE');
        break;
      case 'nl':
        return Locale('nl', 'NL');
        break;
      default:
        return locale;
    }
  }
  return locale;
}

Future<void> initServices() async {
  /// Here is where you put get_storage, hive, shared_pref initialization.
  /// or moor connection, or whatever that's async.

  Get.put<RestApiClient>(
    RestApiClient(
      restApiInterceptor: Get.put<RestApiInterceptor>(
        RestApiInterceptor(),
      ),
    ),
  );

  Get.put<User>(
    User(),
  );

  // Get.put<NewsController>(NewsController());
  print('All services started !');
}
