// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TeamDto _$_$_TeamDtoFromJson(Map<String, dynamic> json) {
  return _$_TeamDto(
    id: json['id'] as String,
    nameTeams: json['name_teams'] as String,
    pathLogoTeams: json['path_logo_team'] as String,
    jeu: json['jeu'] as String,
    listPlayers: (json['list_players'] as List)
        ?.map((e) =>
            e == null ? null : PlayerDto.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    listPalmares: (json['palmares'] as List)
        ?.map((e) =>
            e == null ? null : PalmaresDto.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_TeamDtoToJson(_$_TeamDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name_teams': instance.nameTeams,
      'path_logo_team': instance.pathLogoTeams,
      'jeu': instance.jeu,
      'list_players': instance.listPlayers,
      'palmares': instance.listPalmares,
    };
