import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/scaffold/loading_scaffold.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_views/create_article.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'manager_view_controller.dart';
import 'manager_views/create_sponsor.dart';
import 'manager_views/teams_change.dart';

class ManagerView extends GetView<ManagerViewController> {
  ManagerView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onLoading: LoadingScaffold(),
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      showScrollbar: false,
      body: LayoutBuilder(
        builder: (contextLayout, constraints) => ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: contextLayout.height - 105,
          ),
          child: Container(
            margin: EdgeInsets.only(
                left:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0,
                right:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0),
            // height: contextLayout.height - 105,
            color: Color(0xFFF262626),
            child: Center(
              child: Container(
                height: contextLayout.height,
                width: 1000,
                child: Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Scrollbar(
                    controller: controller.controllerScroll,
                    child: NestedScrollView(
                      controller: controller.controllerScroll,
                      headerSliverBuilder: (context, value) {
                        return [
                          SliverToBoxAdapter(
                            child: Container(
                              decoration: BoxDecoration(
                                  //This is for background color
                                  color: Colors.white.withOpacity(0.0),
                                  //This is for bottom border that is needed
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey, width: 0.8))),
                              child: TabBar(
                                labelPadding:
                                    EdgeInsets.only(top: 10, bottom: 10),
                                overlayColor: MaterialStateProperty.all<Color>(
                                    Colors.lightBlue),

                                labelColor: Colors.white,
                                labelStyle:
                                    kTextStyle.copyWith(color: Colors.white),
                                unselectedLabelColor: Colors.white,
                                unselectedLabelStyle:
                                    kTextStyle.copyWith(color: Colors.white),
                                //     TextStyle(color: Colors.black),
                                isScrollable: false,
                                indicator: BoxDecoration(
                                    color: Colors.black.withOpacity(0.5)),
                                // indicatorColor: Colors.black,
                                physics: NeverScrollableScrollPhysics(),
                                controller: controller.tabController,
                                onTap: (value) {},
                                tabs: List.generate(
                                  controller.tabs.length,
                                  (index) => Container(
                                    child: Text(
                                      controller.tabs[index],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ];
                      },
                      body: Container(
                        // height: 1000,
                        child: TabBarView(
                          controller: controller.tabController,
                          children: [
                            TeamsChange(),
                            CreateArticle(),
                            CreateSponsor(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
