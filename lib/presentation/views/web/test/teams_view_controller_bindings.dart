import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/teams/controller/teams_controller.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/teams_repository_impl.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'teams_view_controller.dart';

class TestViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => TestViewController(
          teamRepo: Get.put(TeamRepositoryImpl()),
          teamsController: Get.put<TeamsController>(TeamsController())),
    );
  }
}
