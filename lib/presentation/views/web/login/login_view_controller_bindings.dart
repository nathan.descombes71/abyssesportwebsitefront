import 'package:abyssesportwebsite/domain/auth/auth_controller.dart';
import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/controller/sponsor_controller.dart';
import 'package:abyssesportwebsite/presentation/views/web/accueil/accueil_view_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'login_view_controller.dart';

class LoginViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => LoginViewController(
        authController: Get.put<AuthController>(
          AuthController(),
        ),
      ),
    );
  }
}
