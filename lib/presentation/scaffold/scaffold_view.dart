import 'package:abyssesportwebsite/domain/feature/user/entities/user.dart'
    as entities;
import 'package:abyssesportwebsite/presentation/core/widgets/animation.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/scheduler/ticker.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'dart:js' as js;

import '../core/styles/styles.dart';
import 'scaffold_content/scaffold_triangle.dart';

class ScaffoldView extends StatefulWidget {
  final Widget body;
  final bool showScrollbar;
  final bool isCharged;
  // final Widget floatingActionButton;
  ScaffoldView({
    Key key,
    this.body,
    this.showScrollbar = true,
    this.isCharged = true,
  }) : super(key: key);

  @override
  _ScaffoldViewState createState() => _ScaffoldViewState();
}

class _ScaffoldViewState extends State<ScaffoldView>
    with TickerProviderStateMixin {
  ScrollController scrollController = ScrollController();
  RxBool isdescendre = false.obs;
  RxDouble sizeLogo = 90.0.obs;
  Animation<double> myAnimation;
  AnimationController animationController;
  Rx<Color> colorIconLogin = Colors.white.obs;
  Rx<entities.User> user = Get.find<entities.User>().obs;

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;

  List<ItemAppBar> listItem = [];

  bool isMenuContext = false;

  double heightMenu = 0.0;
  double widthMenu = 0.0;
  double opawidget = 0.0;

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );

    myAnimation =
        CurvedAnimation(curve: Curves.linear, parent: animationController);

    listItem
      ..add(ItemAppBar(id: 2, name: 'Se déconnecter'))
      ..add(ItemAppBar(id: 3, name: 'Manager'));

    // if (user?.value?.isManager != null && user.value.isManager) {
    //   listItem.add(ItemAppBar(id: 3, name: 'manager'));
    // }
    if (widget.isCharged) {
      Future.delayed(Duration(milliseconds: 10)).then((value) => setState(() {
            opawidget = 1;
          }));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return buildContent(context);
  }

// Image.asset('assets/graph/banniere2.png'),
  Widget buildContent(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (context.width > 700) {
          heightMenu = 0;
          animationController.reverse();
          isMenuContext = false;
        }
        return Scaffold(
          // floatingActionButton: widget.floatingActionButton,
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/graph/backgroundweb.png'),
                  ),
                ),
              ),
              Scrollbar(
                controller: scrollController,
                isAlwaysShown: widget.showScrollbar,
                child: CustomScrollView(
                  controller: scrollController,
                  slivers: [
                    if (Get.currentRoute == Routes.ACCUEIL &&
                        context.width > 700)
                      SliverToBoxAdapter(
                        child: Container(
                          height: 500,
                          child: Center(
                            child: Container(
                              // color: Colors.red,
                              child: AnimatedOpacity(
                                opacity: opawidget,
                                duration: Duration(milliseconds: 2000),
                                child: Row(
                                  children: [
                                    Image.asset('assets/graph/Abyss.png'),
                                    Container(
                                      color: Colors.white,
                                      width: 2,
                                      height: 140,
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Container(
                                      height: 200,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Abyss eSport'.toUpperCase(),
                                            style: kTitleStyle.copyWith(
                                                fontStyle: FontStyle.normal,
                                                fontSize: 50),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            '"Never give up"',
                                            style: TextStyle(
                                                fontStyle: FontStyle.italic,
                                                fontSize: 30),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              height: 300,
                              width: 700,
                            ),
                          ),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage('assets/graph/banniere22.png'),
                            ),
                          ),
                        ),
                      ),
                    SliverAppBar(
                      leading: SizedBox(),
                      toolbarHeight: 105,
                      backgroundColor: Colors.black,
                      pinned: true,
                      flexibleSpace: appBar(),
                    ),
                    SliverToBoxAdapter(
                      child: widget.body,
                    ),
                  ],
                ),
              ),
              AnimatedPositioned(
                top: 105,
                left: 0,
                height: heightMenu,
                width: context.width,
                duration: Duration(milliseconds: 200),
                child: Container(
                  color: Colors.black,
                  child: Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        itemAppBar('home'.tr, '/accueil'),
                        SizedBox(height: 20),
                        itemAppBar('TEAMS', '/teams'),
                        SizedBox(height: 20),
                        itemAppBar('NEWS', '/news'),
                        SizedBox(height: 20),
                        itemAppBar('CONTACT', '/contact'),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget appBar() {
    return LayoutBuilder(
      builder: (contextLayout, constraints) => Column(
        children: [
          Container(
            height: 5,
            width: contextLayout.width,
            color: Color(0xFFFD700FF),
          ),
          Container(
              height: 100,
              width: contextLayout.width,
              color: Colors.black,
              child: contextLayout.width > 700
                  ? Row(children: [
                      Expanded(
                        child: rowIconLeft(contextLayout),
                      ),
                      Expanded(
                        flex: 3,
                        child: Row(
                          children: [
                            Expanded(
                              child: itemAppBar('home'.tr, '/accueil'),
                            ),
                            Expanded(
                              child: itemAppBar('TEAMS', '/teams'),
                            ),
                            contextLayout.width > 1200
                                ? Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 20, left: 20),
                                      child: Transform.scale(
                                        scale: 1.5,
                                        child: Stack(
                                          alignment:
                                              AlignmentDirectional.topCenter,
                                          children: [
                                            Positioned(
                                              top: 0,
                                              child: InkWell(
                                                onTap: () {
                                                  Get.offAndToNamed(
                                                      Routes.ACCUEIL);
                                                },
                                                onHover: (hovering) {
                                                  if (hovering) {
                                                    sizeLogo.value = 100;
                                                  } else {
                                                    sizeLogo.value = 90;
                                                  }
                                                },
                                                child: Obx(
                                                  () => AnimatedContainer(
                                                    width: sizeLogo.value,
                                                    height: sizeLogo.value,
                                                    duration: Duration(
                                                        milliseconds: 100),
                                                    child: Image.asset(
                                                      'assets/graph/ecriture.png',
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            Expanded(
                              child: itemAppBar('NEWS', '/news'),
                            ),
                            Expanded(
                              child: itemAppBar('CONTACT', '/contact'),
                            ),
                          ],
                        ),
                      ),
                      FirebaseAuth.instance.currentUser == null
                          ? Expanded(
                              child: rowIconRight(context),
                            )
                          : Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  IconButton(
                                      color: Colors.white,
                                      icon: Icon(MdiIcons.account),
                                      onPressed: () {
                                        // FirebaseAuth.instance.signOut();
                                        setState(() {});
                                      }),
                                  PopupMenuButton<ItemAppBar>(
                                    onSelected: (value) {
                                      if (value.id == 1) {
                                        // Get.toNamed(Routes.MYACCOUNT);
                                      } else if (value.id == 2) {
                                        FirebaseAuth.instance.signOut();
                                        setState(() {});
                                        succesSnackBar(
                                            text: 'Déconnexion réussi !');
                                      } else if (value.id == 3) {
                                        Get.offAndToNamed(Routes.MANAGER);
                                      }
                                    },
                                    icon: Icon(
                                      MdiIcons.chevronDown,
                                      color: Colors.white,
                                    ),
                                    itemBuilder: (_) {
                                      return listItem.map(
                                        (ItemAppBar e) {
                                          return PopupMenuItem<ItemAppBar>(
                                            value: e,
                                            child: Text(e.name),
                                          );
                                        },
                                      ).toList();
                                    },
                                  ),
                                ],
                              ),
                            ),
                    ])
                  : Row(
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Expanded(
                          flex: 1,
                          child: InkWell(
                            onTap: () {
                              if (!isMenuContext) {
                                animationController.forward();
                                isMenuContext = true;

                                setState(() {
                                  heightMenu = 250;
                                  // widthMenu = 700;
                                });
                              } else {
                                animationController.reverse();
                                isMenuContext = false;

                                setState(() {
                                  widthMenu = 0;
                                  heightMenu = 0;
                                });
                              }
                            },
                            child: AnimatedIcon(
                              color: Colors.white,
                              icon: AnimatedIcons.menu_arrow,
                              progress: myAnimation,
                            ),
                          ),
                        ),
                        // Container(
                        //   width: 200,
                        //   height: 50,
                        //   child: rowIcon(contextLayout),
                        // ),
                        Expanded(
                          child: SizedBox(),
                        ),
                        Container(
                          width: 200,
                          height: 50,
                          child: rowIconAll(contextLayout),
                        ),
                      ],
                    )),
        ],
      ),
    );
  }

  Widget itemAppBar(String text, String namedRoute) {
    Rx<TextStyle> textStyle = TextStyle().obs;
    if (Get.currentRoute == namedRoute) {
      print(Get.currentRoute);
      print(namedRoute);
      textStyle.value = TextStyle(color: Color(0xFFF928199), fontSize: 23);
    } else {
      textStyle.value = TextStyle(color: kTitleColorTransparent, fontSize: 20);
    }
    return Obx(
      () => InkWell(
        onTap: () {
          Get.offAndToNamed(namedRoute);
          print('redirect to : ' + namedRoute);
        },
        onHover: (isHovering) {
          if (isHovering) {
            textStyle.value =
                TextStyle(color: Color(0xFFF928199), fontSize: 23);
            print(itemAppbarStyle.value);
          } else {
            if (Get.currentRoute != namedRoute) {
              textStyle.value =
                  TextStyle(color: kTitleColorTransparent, fontSize: 20);
            }
          }
        },
        child: Center(
            child: AnimatedDefaultTextStyle(
          style: textStyle.value,
          duration: Duration(milliseconds: 100),
          child: Text(
            text,
          ),
        )),
      ),
    );
  }

  Widget rowIconRight(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SizedBox(),
        ),
        Expanded(
          flex: context.width > 1200 ? 2 : 4,
          child: Container(
            child: Row(
              children: [
                SizedBox(
                  width: 5,
                ),
                SizedBox(
                  width: 5,
                ),
                IconButton(
                  color: Colors.white,
                  onPressed: () {},
                  icon: Icon(MdiIcons.youtube),
                ),
                SizedBox(
                  width: 5,
                ),
                IconButton(
                  color: Colors.white,
                  onPressed: () {
                    _launchURL('https://discord.gg/CB9yE8A93K');
                  },
                  icon: Icon(MdiIcons.discord),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: SizedBox(),
        ),
      ],
    );
  }

  Widget rowIconLeft(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SizedBox(),
        ),
        Expanded(
          flex: context.width > 1200 ? 2 : 4,
          child: Container(
            child: Row(
              children: [
                IconButton(
                  color: Colors.white,
                  onPressed: () {
                    _launchURL('https://twitter.com/TeamAbyssEsport');
                  },
                  icon: Icon(MdiIcons.twitter),
                ),
                SizedBox(
                  width: 5,
                ),
                IconButton(
                  color: Colors.white,
                  onPressed: () {
                    _launchURL('https://www.twitch.tv/teamabyssesport');
                  },
                  icon: Icon(MdiIcons.twitch),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: SizedBox(),
        ),
      ],
    );
  }

  Widget rowIconAll(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SizedBox(),
        ),
        Expanded(
          flex: context.width > 1200 ? 2 : 4,
          child: Container(
            child: Row(
              children: [
                Expanded(
                  child: IconButton(
                    color: Colors.white,
                    onPressed: () {
                      _launchURL('https://twitter.com/TeamAbyssEsport');
                    },
                    icon: Icon(MdiIcons.twitter),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: IconButton(
                    color: Colors.white,
                    onPressed: () {
                      _launchURL('https://www.twitch.tv/teamabyssesport');
                    },
                    icon: Icon(MdiIcons.twitch),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: IconButton(
                    color: Colors.white,
                    onPressed: () {},
                    icon: Icon(MdiIcons.youtube),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: IconButton(
                    color: Colors.white,
                    onPressed: () {
                      _launchURL('https://discord.gg/CB9yE8A93K');
                    },
                    icon: Icon(MdiIcons.discord),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: SizedBox(),
        ),
      ],
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}

class ItemAppBar {
  int id;
  String name;

  ItemAppBar({this.id, this.name});
}
