import 'package:abyssesportwebsite/domain/feature/sponsor/repositories/sponsor_repositories.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/sponsor_dto/sponsor_dto.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/rest_api_repository_factory.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'dart:convert' as convert;

import '../api/rest_api_client.dart';

class SponsorRepositoryImpl extends RestApiRepositoryFactory<SponsorDto>
    implements SponsorRepository {
  SponsorRepositoryImpl({
    GetHttpClient client,
  }) : super(
          controller: '/sponsor',
          client: client ?? Get.find<RestApiClient>().client,
        );

  @override
  Future<List<SponsorDto>> getSponsorDtoList(
      {Map<String, dynamic> queryParameters}) async {
    try {
      List<SponsorDto> sponsorList = [];

      var response = await client.get('$controller');

      print('1');

      if (response.body != null) {
        var jsonResponse = convert.jsonDecode(response.body);
        print(convert.jsonDecode(response.body));
        sponsorList = jsonResponse['data'].map<SponsorDto>((e) {
          return SponsorDto.fromJson(e);
        }).toList();
      }
      return sponsorList;
    } catch (e) {
      print(e.toString());
      throw (e);
      // errorSnackBar(text: e.toString());
    }
  }

  @override
  Future<String> createSponsor(SponsorDto sponsor) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);

      var response = await client
          .post('$controller/create', body: sponsor.toJson(), headers: {
        "Authorization": "Bearer " + token,
      });
      return response.body;
    } catch (e) {
      throw (e);
    }
  }

  @override
  Future<String> deleteSponsor(int idSponsor) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);

      var response = await client.delete('$controller/$idSponsor', headers: {
        "Authorization": "Bearer " + token,
      });

      return response.body;
    } catch (e) {
      throw (e);
    }
  }
}
