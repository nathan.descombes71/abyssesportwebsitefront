import 'dart:js';

import 'package:abyssesportwebsite/domain/feature/teams/entities/teams.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/scaffold/loading_scaffold.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/teams/teams_content/teams_player_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'teams_view_controller.dart';

class TeamsView extends GetView<TeamsViewController> {
  TeamsView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => buildContent(context),
        onLoading: LoadingScaffold());
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: Container(
        decoration: BoxDecoration(
          // color: Color(0xFFF262626),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/graph/backgroundtest.png'),
          ),
        ),

        // height: 800,
        child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: context.height - 105),
          child: LayoutBuilder(
            builder: (contextLayout, constraints) => Obx(
              () => Column(
                children: [
                  Stack(
                    children: [
                      Positioned.fill(
                          // left: contextLayout.width * 0.333,
                          top: 0,
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: 150,
                              width: 3,
                              color: Colors.white.withOpacity(0.5),
                            ),
                          )),
                      Container(
                        // color: Colors.red,
                        margin: EdgeInsets.fromLTRB(0, 100, 0, 100),
                        height: 100,
                        // width: 300,
                        child: _header(contextLayout, controller.listAllTeams),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: contextLayout.width * 0.07),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Container(
                            height: 80,
                            width: 5,
                            color: Colors.white.withOpacity(0.3),
                          ),
                          SizedBox(width: 20),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Obx(
                                () => SelectableText(
                                  //Si nom de team et nom de jeu pas égal = ecrire nom de team (example valo academy)
                                  controller.teamShow.value.jeu !=
                                          controller.teamShow.value.nameTeams
                                      ? controller.teamShow.value.nameTeams
                                          .toUpperCase()
                                      : controller.teamShow.value.jeu
                                          .toUpperCase(),
                                  style: kTitleStyle,
                                ),
                              ),
                              SelectableText(
                                'players'.tr,
                                style: kTitleStyle.copyWith(fontSize: 25),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          IntrinsicWidth(
                            child: Container(
                              // color: Colors.red,
                              child: Row(
                                children: [
                                  Obx(
                                    () => Checkbox(
                                      activeColor: Color(0xFFFD700FF),
                                      value: controller.canSee.value,
                                      onChanged: (value) {
                                        controller.canSee.value = value;
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text('seePlayersInfo'.tr),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Obx(
                    () => Wrap(
                      children: [
                        for (var i = 0;
                            i < controller.teamShow.value.listPlayers.length;
                            i++)
                          Padding(
                            padding: EdgeInsets.all(30),
                            child: TeamsPlayerCard(
                              actualPlayers:
                                  controller.teamShow.value.listPlayers[i],
                              canSeePlayers: controller.canSee,
                            ),
                          ),
                      ],
                    ),
                  ),
                  controller.teamShow.value.listPalmares != null &&
                          controller.teamShow.value.listPalmares.isNotEmpty
                      ? Container(
                          padding: EdgeInsets.only(
                            left: contextLayout.width * 0.07,
                            top: 50,
                            right: contextLayout.width * 0.07,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    height: 60,
                                    width: 5,
                                    color: Colors.white.withOpacity(0.3),
                                  ),
                                  SizedBox(width: 20),
                                  SelectableText(
                                    "awards".tr,
                                    style: kTitleStyle,
                                  ),
                                ],
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(maxHeight: 500),
                                child: Container(
                                  width: 400,
                                  margin: EdgeInsets.only(
                                      left: 40, top: 50, bottom: 50),
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.4),
                                    border: Border.all(
                                        color: Color(0xFFFD700FF), width: 3),
                                  ),
                                  child: Obx(
                                    () => ListView.builder(
                                        itemCount: controller
                                            .teamShow.value.listPalmares.length,
                                        itemBuilder: (contextListView, i) {
                                          return Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                20, 10, 0, 0),
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  height: 20,
                                                  child: controller
                                                      .teamShow
                                                      .value
                                                      .listPalmares[i]
                                                      .positionImage,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                SelectableText(
                                                  controller.teamShow.value
                                                      .listPalmares[i].nameCup,
                                                ),
                                              ],
                                            ),
                                          );
                                        }),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _header(BuildContext contextLayout, RxList<Teams> teamsList) {
    return Obx(
      () => ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: teamsList.length +
            (controller.selectionnedItem.value -
                controller.selectionnedItem.value),
        itemBuilder: (contextt, i) {
          Rx<Color> borderColor = Colors.black.obs;
          Rx<Color> backgroundColor = Color(0xFF7a3f8a).obs;
          Rx<Color> logoColor = Colors.black.obs;
          if (controller.selectionnedItem.value == i) {
            borderColor = Color(0xFF7a3f8a).obs;
            backgroundColor = Colors.black.obs;
            logoColor = Color(0xFF7a3f8a).obs;
          } else {
            borderColor = Colors.black.obs;
            backgroundColor = Color(0xFF7a3f8a).obs;
            logoColor = Colors.black.obs;
          }
          return Container(
            height: 100,
            width: i != controller.listAllTeams.length - 1
                ? contextLayout.width * 0.2
                : 100,
            child: Stack(
              children: [
                if (i != controller.listAllTeams.length - 1)
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(left: 30),
                      color: Colors.white.withOpacity(0.5),
                      height: 3,
                      width: contextLayout.width * 0.2,
                    ),
                  ),
                Positioned(
                  left: 0,
                  child: InkWell(
                    onTap: () {
                      controller.selectionnedItem.value = i;
                      controller.teamShow.value = controller.listAllTeams[i];
                      // controller.refresh();
                    },
                    onHover: (isHover) {
                      if (isHover) {
                        print(i);
                        backgroundColor.value = Colors.black;
                        logoColor.value = Color(0xFF7a3f8a);
                        borderColor.value = Color(0xFF7a3f8a);
                      } else {
                        if (controller.selectionnedItem.value != i) {
                          backgroundColor.value = Color(0xFF7a3f8a);
                          logoColor.value = Colors.black;
                          borderColor.value = Colors.black;
                        }
                      }
                    },
                    child: Obx(
                      () => AnimatedContainer(
                        duration: Duration(milliseconds: 100),
                        decoration: BoxDecoration(
                            color: backgroundColor.value,
                            border:
                                Border.all(color: borderColor.value, width: 7)),
                        padding: EdgeInsets.all(10),
                        height: 100,
                        width: 100,
                        child: Image.network(
                          controller.listAllTeams[i].pathLogoTeam,
                          color: logoColor.value,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
