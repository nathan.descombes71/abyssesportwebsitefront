import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'create_article_view_controller.dart';

class CreateArticleView extends GetView<CreateArticleViewController> {
  CreateArticleView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: LayoutBuilder(
        builder: (contextLayout, constraints) => ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: contextLayout.height - 105,
          ),
          child: Container(
            margin: EdgeInsets.only(
                left:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0,
                right:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0),
            // height: contextLayout.height - 105,
            color: Color(0xFFF262626),
            child: Center(
              child: Container(
                // height: 300,
                width: 400,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 60,
                          width: 5,
                          color: Colors.white.withOpacity(0.3),
                        ),
                        SizedBox(width: 20),
                        SelectableText(
                          "CREER UN ARTICLE",
                          style: kTitleStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    TextFormField(
                      style: TextStyle(color: Colors.white),
                      // obscureText: true,
                      onChanged: (value) {
                        controller.pass = value;
                      },
                      decoration: InputDecoration(
                        labelText: "Titre",
                        // fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      // initialValue: 'test',
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )),
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Color(0xFF8338a1),
                        ),
                      ),
                      onPressed: () {},
                      child: Text('Ajouter une photo'),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      // height: 1000,
                      width: 600,
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.listParagraph.length,
                          itemBuilder: (context, index) {
                            return Container(
                              height: 150,
                              width: 500,
                              child: Column(
                                children: [
                                  TextFormField(
                                    maxLines: 4,
                                    style: TextStyle(color: Colors.white),
                                    // obscureText: true,
                                    onChanged: (value) {
                                      controller.pass = value;
                                    },
                                    decoration: InputDecoration(
                                      hintText: "Paragraphe " +
                                          (index + 1).toString(),
                                      // fillColor: Colors.white,
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            new BorderRadius.circular(25.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    // initialValue: 'test',
                                  ),
                                ],
                              ),
                            );
                          }),
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )),
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Color(0xFF8338a1),
                        ),
                      ),
                      onPressed: () {
                        controller.listParagraph.add('');
                        controller.refresh();
                      },
                      child: Text('Ajouter un paragraphe'),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )),
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Color(0xFF8338a1),
                        ),
                      ),
                      onPressed: () {},
                      child: Text('Envoyer l\'article'),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
