// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'palmares_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PalmaresDto _$_$_PalmaresDtoFromJson(Map<String, dynamic> json) {
  return _$_PalmaresDto(
    id: json['id'] as int,
    nameCup: json['name'] as String,
    position: json['position'] as int,
  );
}

Map<String, dynamic> _$_$_PalmaresDtoToJson(_$_PalmaresDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.nameCup,
      'position': instance.position,
    };
