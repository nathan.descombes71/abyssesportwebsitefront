import 'package:abyssesportwebsite/infrastructure/dtos/palmares_dto/palmares_dto.dart';
import 'package:flutter/material.dart';

class Palmares {
  int id;
  String nameCup;
  int position;

  Widget get positionImage {
    switch (position) {
      case 1:
        return Image.asset('assets/icon/first_place.png');
        break;
      case 2:
        return Image.asset('assets/icon/second_place.png');
        break;
      case 3:
        return Image.asset('assets/icon/third_place.png');
        break;
      default:
        return Text("#" + position.toString());
    }
  }

  Palmares({
    this.id,
    this.nameCup,
    this.position,
  });
}

extension OnPalmares on Palmares {
  Palmares copyWith({
    int id,
    String nameCup,
    int position,
  }) {
    return Palmares(
      id: id ?? this.id,
      nameCup: nameCup ?? this.nameCup,
      position: position ?? this.position,
    );
  }

    PalmaresDto get toDto {
    return PalmaresDto(
      id: id,
      nameCup: nameCup,
      position: position,
      );
  }
}

extension OnListPalmares on List<Palmares> {
  List<PalmaresDto> get toDto {
    List<PalmaresDto> _palmaresDtoList = [];

    this?.forEach((entity) => _palmaresDtoList.add(entity.toDto));
    return _palmaresDtoList;
  }
}

extension OnListPalmaresDto on List<PalmaresDto> {
  List<Palmares> get toEntity {
    List<Palmares> _palmaresList = [];

    this?.forEach((dto) => _palmaresList.add(dto.toEntity));
    return _palmaresList;
  }
}

extension OnPalmaresDto on PalmaresDto {
  Palmares get toEntity {
    return Palmares(
      id: id,
      nameCup: nameCup,
      position: position,
    );
  }
}