import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/core/validator/validator.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'contact_view_controller.dart';

class ContactView extends GetView<ContactViewController> {
  ContactView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: LayoutBuilder(
        builder: (contextLayout, constraints) => ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: contextLayout.height - 105,
          ),
          child: Container(
            decoration: BoxDecoration(
              // color: Color(0xFFF262626),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/graph/backgroundtest.png'),
              ),
            ),
            // height: contextLayout.height - 105,

            child: Center(
              child: Container(
                // height: 600,
                width: 600,
                child: Form(
                  key: controller.formKey,
                  child: Column(
                    children: [
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 60,
                            width: 5,
                            color: Colors.white.withOpacity(0.3),
                          ),
                          SizedBox(width: 20),
                          SelectableText(
                            'CONTACT',
                            style: kTitleStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      // Expanded(
                      //   child: Row(
                      //     children: [TextFormField(), TextFormField()],
                      //   ),
                      // )
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: TextFormField(
                              onChanged: (value) {
                                controller.nom = value;
                              },
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "lastName".tr,
                                // fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              // initialValue: 'test',
                            ),
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          Expanded(
                            child: TextFormField(
                              onChanged: (value) {
                                controller.prenom = value;
                              },
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "firstName".tr,
                                // fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              // initialValue: 'test',
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "Pseudo",
                                // fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              onChanged: (value) {
                                controller.pseudo = value;
                              },
                            ),
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: controller.controllerObjectText,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'objectFieldError'.tr;
                                }
                                return null;
                              },
                              onChanged: (value) {
                                controller.objet = value;
                              },
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "object".tr + "*",
                                // fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (!Validator.validEmail(value)) {
                            return 'emailFieldError'.tr;
                          }
                          return null;
                        },
                        onChanged: (value) {
                          controller.email = value;
                        },
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "mail".tr + "*",
                          // fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'messageFieldError'.tr;
                          }
                          return null;
                        },
                        onChanged: (value) {
                          controller.messageMail = value;
                        },
                        maxLines: 8,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "yourMessage".tr + '*',
                          // fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Color(0xFF7a3f8a),
                          onSurface: Colors.grey,
                        ),
                        onPressed: () {
                          if (controller.formKey.currentState.validate()) {
                            controller.sendEmail();
                          } else {
                            print('Impossible');
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.all(8),
                          child: Text('send'.tr),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
