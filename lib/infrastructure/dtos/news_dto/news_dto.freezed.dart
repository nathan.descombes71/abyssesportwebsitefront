// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'news_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
NewsDto _$NewsDtoFromJson(Map<String, dynamic> json) {
  return _NewsDto.fromJson(json);
}

/// @nodoc
class _$NewsDtoTearOff {
  const _$NewsDtoTearOff();

// ignore: unused_element
  _NewsDto call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'author') String author,
      @JsonKey(name: 'link_image') String linkImage,
      @JsonKey(name: 'paragraph') List<String> paragraph,
      @JsonKey(name: 'created_at') String createdAt,
      @JsonKey(name: 'title') String title}) {
    return _NewsDto(
      id: id,
      author: author,
      linkImage: linkImage,
      paragraph: paragraph,
      createdAt: createdAt,
      title: title,
    );
  }

// ignore: unused_element
  NewsDto fromJson(Map<String, Object> json) {
    return NewsDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $NewsDto = _$NewsDtoTearOff();

/// @nodoc
mixin _$NewsDto {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'author')
  String get author;
  @JsonKey(name: 'link_image')
  String get linkImage;
  @JsonKey(name: 'paragraph')
  List<String> get paragraph;
  @JsonKey(name: 'created_at')
  String get createdAt;
  @JsonKey(name: 'title')
  String get title;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $NewsDtoCopyWith<NewsDto> get copyWith;
}

/// @nodoc
abstract class $NewsDtoCopyWith<$Res> {
  factory $NewsDtoCopyWith(NewsDto value, $Res Function(NewsDto) then) =
      _$NewsDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'author') String author,
      @JsonKey(name: 'link_image') String linkImage,
      @JsonKey(name: 'paragraph') List<String> paragraph,
      @JsonKey(name: 'created_at') String createdAt,
      @JsonKey(name: 'title') String title});
}

/// @nodoc
class _$NewsDtoCopyWithImpl<$Res> implements $NewsDtoCopyWith<$Res> {
  _$NewsDtoCopyWithImpl(this._value, this._then);

  final NewsDto _value;
  // ignore: unused_field
  final $Res Function(NewsDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object author = freezed,
    Object linkImage = freezed,
    Object paragraph = freezed,
    Object createdAt = freezed,
    Object title = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      author: author == freezed ? _value.author : author as String,
      linkImage: linkImage == freezed ? _value.linkImage : linkImage as String,
      paragraph:
          paragraph == freezed ? _value.paragraph : paragraph as List<String>,
      createdAt: createdAt == freezed ? _value.createdAt : createdAt as String,
      title: title == freezed ? _value.title : title as String,
    ));
  }
}

/// @nodoc
abstract class _$NewsDtoCopyWith<$Res> implements $NewsDtoCopyWith<$Res> {
  factory _$NewsDtoCopyWith(_NewsDto value, $Res Function(_NewsDto) then) =
      __$NewsDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'author') String author,
      @JsonKey(name: 'link_image') String linkImage,
      @JsonKey(name: 'paragraph') List<String> paragraph,
      @JsonKey(name: 'created_at') String createdAt,
      @JsonKey(name: 'title') String title});
}

/// @nodoc
class __$NewsDtoCopyWithImpl<$Res> extends _$NewsDtoCopyWithImpl<$Res>
    implements _$NewsDtoCopyWith<$Res> {
  __$NewsDtoCopyWithImpl(_NewsDto _value, $Res Function(_NewsDto) _then)
      : super(_value, (v) => _then(v as _NewsDto));

  @override
  _NewsDto get _value => super._value as _NewsDto;

  @override
  $Res call({
    Object id = freezed,
    Object author = freezed,
    Object linkImage = freezed,
    Object paragraph = freezed,
    Object createdAt = freezed,
    Object title = freezed,
  }) {
    return _then(_NewsDto(
      id: id == freezed ? _value.id : id as int,
      author: author == freezed ? _value.author : author as String,
      linkImage: linkImage == freezed ? _value.linkImage : linkImage as String,
      paragraph:
          paragraph == freezed ? _value.paragraph : paragraph as List<String>,
      createdAt: createdAt == freezed ? _value.createdAt : createdAt as String,
      title: title == freezed ? _value.title : title as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_NewsDto implements _NewsDto {
  const _$_NewsDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'author') this.author,
      @JsonKey(name: 'link_image') this.linkImage,
      @JsonKey(name: 'paragraph') this.paragraph,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'title') this.title});

  factory _$_NewsDto.fromJson(Map<String, dynamic> json) =>
      _$_$_NewsDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'author')
  final String author;
  @override
  @JsonKey(name: 'link_image')
  final String linkImage;
  @override
  @JsonKey(name: 'paragraph')
  final List<String> paragraph;
  @override
  @JsonKey(name: 'created_at')
  final String createdAt;
  @override
  @JsonKey(name: 'title')
  final String title;

  @override
  String toString() {
    return 'NewsDto(id: $id, author: $author, linkImage: $linkImage, paragraph: $paragraph, createdAt: $createdAt, title: $title)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NewsDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.author, author) ||
                const DeepCollectionEquality().equals(other.author, author)) &&
            (identical(other.linkImage, linkImage) ||
                const DeepCollectionEquality()
                    .equals(other.linkImage, linkImage)) &&
            (identical(other.paragraph, paragraph) ||
                const DeepCollectionEquality()
                    .equals(other.paragraph, paragraph)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(author) ^
      const DeepCollectionEquality().hash(linkImage) ^
      const DeepCollectionEquality().hash(paragraph) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(title);

  @JsonKey(ignore: true)
  @override
  _$NewsDtoCopyWith<_NewsDto> get copyWith =>
      __$NewsDtoCopyWithImpl<_NewsDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_NewsDtoToJson(this);
  }
}

abstract class _NewsDto implements NewsDto {
  const factory _NewsDto(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'author') String author,
      @JsonKey(name: 'link_image') String linkImage,
      @JsonKey(name: 'paragraph') List<String> paragraph,
      @JsonKey(name: 'created_at') String createdAt,
      @JsonKey(name: 'title') String title}) = _$_NewsDto;

  factory _NewsDto.fromJson(Map<String, dynamic> json) = _$_NewsDto.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'author')
  String get author;
  @override
  @JsonKey(name: 'link_image')
  String get linkImage;
  @override
  @JsonKey(name: 'paragraph')
  List<String> get paragraph;
  @override
  @JsonKey(name: 'created_at')
  String get createdAt;
  @override
  @JsonKey(name: 'title')
  String get title;
  @override
  @JsonKey(ignore: true)
  _$NewsDtoCopyWith<_NewsDto> get copyWith;
}
