import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class VideoPlease extends StatefulWidget {
  final Widget widget;
  // final Widget floatingActionButton;
  VideoPlease({
    Key key,
    this.widget,
  }) : super(key: key);

  @override
  _VideoPleaseState createState() => _VideoPleaseState();
}

class _VideoPleaseState extends State<VideoPlease>
    with TickerProviderStateMixin {
  YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: 'VoFNIaSoX-U', // passed in to the widget constructor
      params: YoutubePlayerParams(
        showControls: false,
        autoPlay: true,
        // mute: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
    };
    _controller.onExitFullscreen = () {};
  }

  @override
  Widget build(BuildContext context) {
    return YoutubePlayerControllerProvider(
      controller: _controller,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          YoutubePlayerIFrame(
            aspectRatio: 16 / 9,
          ),
          // YoutubeValueBuilder(
          //   builder: (context, value) => ElevatedButton(
          //     child: const Text('Unmute'),
          //     onPressed: () => context.ytController.unMute(),
          //   ),
          // ),
        ],
      ),
    );
  }
}
