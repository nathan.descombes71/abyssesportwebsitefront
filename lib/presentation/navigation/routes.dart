class Routes {
  static String get initialRoute {
    return ACCUEIL;
  }

  static const ACCUEIL = '/accueil';
  static const TEAMS = '/teams';
  static const NEWS = '/news';
  static const CONTACT = '/contact';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const TEST = '/test';
  static const MANAGER = '/manager';
  static const ARTICLE = '/article';
  static const PLAYERRANK = '/playerRank';
}
