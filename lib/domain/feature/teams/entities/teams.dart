import 'package:abyssesportwebsite/domain/feature/players/entities/players.dart';
import 'package:abyssesportwebsite/domain/feature/teams/entities/palmares.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/team_dto/team_dto.dart';

class Teams {
  String id;
  String nameTeams;
  String jeu;
  String pathLogoTeam;
  List<Players> listPlayers;
  List<Palmares> listPalmares;

  Teams({
    this.id,
    this.nameTeams,
    this.jeu,
    this.pathLogoTeam,
    this.listPlayers,
    this.listPalmares,
  });
}

extension OnTeams on Teams {
  Teams copyWith({
    String id,
    String nameTeams,
    String jeu,
    List<Players> listPlayers,
    String pathLogoTeam,
    List<Palmares> listPalmares,
  }) {
    return Teams(
      id: id ?? this.id,
      nameTeams: nameTeams ?? this.nameTeams,
      jeu: jeu ?? this.jeu,
      pathLogoTeam: pathLogoTeam ?? this.pathLogoTeam,
      listPlayers: listPlayers ?? this.listPlayers,
      listPalmares: listPalmares ?? this.listPalmares,
    );
  }
  
  TeamDto get toDto {
    return TeamDto(
      id: id,
      jeu: jeu,
      listPalmares: listPalmares?.toDto,
      listPlayers: listPlayers.toDto,
      nameTeams: nameTeams,
      pathLogoTeams: pathLogoTeam,
      );
  }
}


extension OnListTeams on List<Teams> {
  List<TeamDto> get toDto {
    List<TeamDto> _teamsDtoList = [];

    this?.forEach((entity) => _teamsDtoList.add(entity.toDto));
    return _teamsDtoList;
  }
}

extension OnListTeamsDto on List<TeamDto> {
  List<Teams> get toEntity {
    List<Teams> _teamsList = [];

    this?.forEach((dto) => _teamsList.add(dto.toEntity));
    return _teamsList;
  }
}

extension OnPlayersDto on TeamDto {
  Teams get toEntity {
    return Teams(
      id: id,
     jeu: jeu,
     listPalmares: listPalmares.toEntity,
     listPlayers: listPlayers.toEntity,
     nameTeams: nameTeams,
     pathLogoTeam: pathLogoTeams,
    );
  }
}
