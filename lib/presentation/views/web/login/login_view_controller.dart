import 'package:abyssesportwebsite/domain/auth/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginViewController extends GetxController with StateMixin {
  final AuthController authController;
  LoginViewController({this.authController});

  String email = '';
  String pass = '';
  RxString errorMessage = ''.obs;

  @override
  void onInit() {
    change(
      null,
      status: RxStatus.loading(),
    );

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  Future<String> signIn() async {
    return await authController.signIn(email, pass);
  }
}
