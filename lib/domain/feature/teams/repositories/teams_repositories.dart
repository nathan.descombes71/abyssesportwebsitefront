import 'package:abyssesportwebsite/domain/feature/teams/entities/teams.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/team_dto/team_dto.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/rest_api_repository_factory.dart';
import 'package:dartz/dartz.dart';

abstract class TeamRepository extends RestApiRepositoryFactory<TeamDto> {
  Future<List<TeamDto>> getTeamDtoList({
    Map<String, dynamic> queryParameters,
  });
  Future<String> createTeam(TeamDto teams);
  Future<String> updateTeams(TeamDto teams);
  Future<String> deletePlayer(String idTeams);
}
