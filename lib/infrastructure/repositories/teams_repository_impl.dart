import 'package:abyssesportwebsite/domain/feature/teams/entities/teams.dart';
import 'package:abyssesportwebsite/domain/feature/teams/repositories/teams_repositories.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/team_dto/team_dto.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/rest_api_repository_factory.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import '../api/rest_api_client.dart';

class TeamRepositoryImpl extends RestApiRepositoryFactory<TeamDto>
    implements TeamRepository {
  TeamRepositoryImpl({
    GetHttpClient client,
  }) : super(
          controller: '/teams',
          client: client ?? Get.find<RestApiClient>().client,
        );

  @override
  Future<List<TeamDto>> getTeamDtoList(
      {Map<String, dynamic> queryParameters}) async {
    try {
      List<TeamDto> printt = [];

      var response = await client.get('$controller');

      var jsonResponse = convert.jsonDecode(response.body);

      print(jsonResponse['data']);

      printt = jsonResponse['data'].map<TeamDto>((e) {
        return TeamDto.fromJson(e);
      }).toList();

      return printt;
    } catch (e) {
      print(e.toString());
      throw (e);
      // errorSnackBar(text: e.toString());
    }
  }

  @override
  Future<String> createTeam(TeamDto teams) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);
      var response = await client
          .post('$controller/create', body: teams.toJson(), headers: {
        "Authorization": "Bearer " + token,
      });

      print(teams.toJson());

      return response.body;
    } catch (e) {
      throw (e);
    }
  }

  @override
  Future<String> updateTeams(TeamDto teams) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);

      var response = await client
          .put('$controller/${teams.id}', body: teams.toJson(), headers: {
        "Authorization": "Bearer " + token,
      });

      print(teams.toJson());

      return response.body;
    } catch (e) {
      throw (e);
    }
  }

  @override
  Future<String> deletePlayer(String idTeams) async {
    try {
      String token = await FirebaseAuth.instance.currentUser
          .getIdToken(true)
          .then((value) => value);

      var response = await client.delete('$controller/$idTeams', headers: {
        "Authorization": "Bearer " + token,
      });

      return response.body;
    } catch (e) {
      throw (e);
    }
  }
}
