import 'package:abyssesportwebsite/infrastructure/dtos/news_dto/news_dto.dart';

class News {
  int id;
  String title;
  String linkImage;
  List<String> paragraph;
  String author;
  String createdAt;

  News({
    this.id,
    this.title,
    this.linkImage,
    this.paragraph,
    this.author,
    this.createdAt,
  });
}

extension OnNews on News {
  News copyWith({
    int id,
    String title,
    String linkImage,
    List<String> paragraph,
    String author,
    String createdAt,
  }) {
    return News(
      id: id ?? this.id,
      title: title ?? this.title,
      linkImage: linkImage ?? this.linkImage,
      paragraph: paragraph ?? this.paragraph,
      author: author ?? this.author,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  NewsDto get toDto {
    return NewsDto(
      id: id,
      title: title,
      linkImage: linkImage,
      paragraph: paragraph,
      author: author,
      createdAt: createdAt,
    );
  }
}

extension OnListNews on List<News> {
  List<NewsDto> get toDto {
    List<NewsDto> _newsDtoList = [];

    this?.forEach((entity) => _newsDtoList.add(entity.toDto));
    return _newsDtoList;
  }
}

extension OnListNewsDto on List<NewsDto> {
  List<News> get toEntity {
    List<News> _newsList = [];

    this?.forEach((dto) => _newsList.add(dto.toEntity));
    return _newsList;
  }
}

extension OnNewsDto on NewsDto {
  News get toEntity {
    return News(
      id: id,
      author: author,
      createdAt: createdAt,
      linkImage: linkImage,
      paragraph: paragraph,
      title: title,
    );
  }
}
