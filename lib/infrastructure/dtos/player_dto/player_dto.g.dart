// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'player_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlayerDto _$_$_PlayerDtoFromJson(Map<String, dynamic> json) {
  return _$_PlayerDto(
    id: json['id'] as int,
    pseudo: json['pseudo'] as String,
    country: json['country'] as String,
    age: json['age'] as int,
    role: json['role'] as String,
    jeu: json['jeu'] as String,
    cardPath: json['card_path'] as String,
    isStaff: json['is_staff'] as bool,
    roleStaff: json['role_staff'] as String,
  );
}

Map<String, dynamic> _$_$_PlayerDtoToJson(_$_PlayerDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'pseudo': instance.pseudo,
      'country': instance.country,
      'age': instance.age,
      'role': instance.role,
      'jeu': instance.jeu,
      'card_path': instance.cardPath,
      'is_staff': instance.isStaff,
      'role_staff': instance.roleStaff,
    };
