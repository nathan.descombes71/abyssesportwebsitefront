import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/accueil/accueil_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/accueil/accueil_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/contact/contact_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/contact/contact_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/create_article/create_article_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/create_article/create_article_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/detail_news/details_niews_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/detail_news/details_niews_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/login/login_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/login/login_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/news/news_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/news/news_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/teams/teams_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/teams/teams_view_controller_bindings.dart';
import 'package:abyssesportwebsite/presentation/views/web/test/teams_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/test/teams_view_controller_bindings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'routes.dart';

class Nav {
  NewsController newsController = NewsController();

  static Route<dynamic> routesGenerate(RouteSettings route) {
    List<String> splitRoute = [];
    String nameRoute = route.name;

    splitRoute = route.name.replaceFirst('/', '').split('/');
    nameRoute = splitRoute[0];
    nameRoute = '/' + nameRoute;

    switch (nameRoute) {
      case Routes.ACCUEIL:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.ACCUEIL),
          routeName: Routes.ACCUEIL,
          page: () => AccueilView(),
          binding: AccueilViewControllerBindings(),
        );
        break;
      case Routes.NEWS:
        if (splitRoute.length < 2) {
          return GetPageRoute(
            settings: RouteSettings(name: Routes.NEWS),
            routeName: Routes.NEWS,
            page: () => NewsView(),
            binding: NewsViewControllerBindings(),
          );
        } else {
          return GetPageRoute(
            settings: RouteSettings(name: route.name),
            routeName: route.name,
            page: () => DetailsNiewsView(),
            binding: DetailsNiewsViewControllerBindings(),
          );
        }
        break;
      case Routes.TEAMS:
        return GetPageRoute(
          settings: RouteSettings(name: route.name),
          routeName: route.name,
          page: () => TeamsView(),
          binding: TeamsViewControllerBindings(),
        );
      case Routes.CONTACT:
        return GetPageRoute(
          settings: RouteSettings(name: route.name),
          routeName: route.name,
          page: () => ContactView(),
          binding: ContactViewControllerBindings(),
        );
        break;
      case Routes.LOGIN:
        return GetPageRoute(
          settings: RouteSettings(name: route.name),
          routeName: route.name,
          page: () => LoginView(),
          binding: LoginViewControllerBindings(),
        );
        break;
      case Routes.TEST:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.TEST),
          routeName: Routes.TEST,
          page: () => TestView(),
          binding: TestViewControllerBindings(),
        );
        break;
      case Routes.MANAGER:
        if (FirebaseAuth.instance.currentUser == null) {
          return GetPageRoute(
            settings: RouteSettings(name: route.name),
            routeName: route.name,
            page: () => LoginView(),
            binding: LoginViewControllerBindings(),
          );
        } else {
          return GetPageRoute(
            settings: RouteSettings(name: Routes.MANAGER),
            routeName: Routes.MANAGER,
            page: () => ManagerView(),
            binding: ManagerViewControllerBindings(),
          );
        }
        break;
      case Routes.ARTICLE:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.ARTICLE),
          routeName: Routes.ARTICLE,
          page: () => CreateArticleView(),
          binding: CreateArticleViewControllerBindings(),
        );
        break;
      case '/':
        return GetPageRoute(
          page: () => ScaffoldView(
            body: Container(),
          ),
        );
      default:
        return GetPageRoute(
          page: () => ScaffoldView(body: Container()),
        );
    }
  }

  static List<GetPage> routes = [
    /// REVIEW [Web] routes

    GetPage(
      name: Routes.ACCUEIL,
      page: () => AccueilView(),
      binding: AccueilViewControllerBindings(),
    ),
    GetPage(
      name: Routes.TEAMS,
      page: () => TeamsView(),
      binding: TeamsViewControllerBindings(),
    ),
    GetPage(
      name: Routes.NEWS,
      page: () => NewsView(),
      binding: NewsViewControllerBindings(),
    ),
  ];
}
