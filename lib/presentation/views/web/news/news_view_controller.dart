import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsViewController extends GetxController with StateMixin {
  NewsViewController({this.newsController});

  final NewsController newsController;

  PageController pageController = PageController();

  Animation<double> myAnimation;
  AnimationController animationController;

  List<News> allNews = [];

  RxDouble sizeLogo = 55.0.obs;

  final RxInt currentIndex = 0.obs;

  @override
  void onInit() async {
    change(
      null,
      status: RxStatus.loading(),
    );
    allNews = await newsController.getNewsList();

    print('allNews lenght' + allNews.length.toString());
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
