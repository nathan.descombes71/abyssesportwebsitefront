import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailsNiewsViewController extends GetxController with StateMixin {
  DetailsNiewsViewController();

  NewsController newsController = NewsController();

  PageController pageController = PageController();

  Animation<double> myAnimation;
  AnimationController animationController;

  Rx<News> actualNews = News().obs;
  List<List<TextSpan>> allParagraph = [];

  RxDouble sizeLogo = 55.0.obs;

  final RxInt currentIndex = 0.obs;
  @override
  void onInit() async {
    // actualNews.value = newsController.allNews.first;

    int idNews = int.parse(Get.currentRoute.split("/").last);
    print(idNews);

    List<News> allNews = await newsController.getNewsList();

    actualNews.value = allNews.firstWhere((element) => element.id == idNews);
    int indexx = 0;
    List<String> wantottest = [];
    actualNews.value.paragraph.forEach((element) {
      allParagraph.add([]);
      wantottest = element.split('**');

      int indexDecompose = 0;
      wantottest.forEach((e) {
        // e.replaceAll('R', '\n');
        if (indexDecompose.isOdd) {
          allParagraph[indexx].add(TextSpan(
              text: e.replaceAll(r'\n', '\n'),
              style: TextStyle(
                  fontStyle: FontStyle.italic, fontWeight: FontWeight.bold)));
        } else {
          allParagraph[indexx].add(TextSpan(
            text: e.replaceAll(r'\n', '\n'),
          ));

          print(e);
          //   allParagraph[indexx].add(TextSpan(
          //   text: '\n',
          // ));
        }
        indexDecompose++;
      });

      indexx = indexx + 1;
    });

// actualNews.value = newsController.allNews.singleWhere((element) => element.id == );

    change(
      null,
      status: RxStatus.loading(),
    );

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void deleteNews() {
    try {
      newsController.deleteNews(actualNews.value.id);

      Get.offAndToNamed(Routes.NEWS);
      succesSnackBar(text: 'News supprimer avec succès');
    } catch (e) {
      throw (e);
    }
  }
}
