// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'player_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
PlayerDto _$PlayerDtoFromJson(Map<String, dynamic> json) {
  return _PlayerDto.fromJson(json);
}

/// @nodoc
class _$PlayerDtoTearOff {
  const _$PlayerDtoTearOff();

// ignore: unused_element
  _PlayerDto call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'pseudo') String pseudo,
      @JsonKey(name: 'country') String country,
      @JsonKey(name: 'age') int age,
      @JsonKey(name: 'role') String role,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'card_path') String cardPath,
      @JsonKey(name: 'is_staff') bool isStaff,
      @JsonKey(name: 'role_staff') String roleStaff}) {
    return _PlayerDto(
      id: id,
      pseudo: pseudo,
      country: country,
      age: age,
      role: role,
      jeu: jeu,
      cardPath: cardPath,
      isStaff: isStaff,
      roleStaff: roleStaff,
    );
  }

// ignore: unused_element
  PlayerDto fromJson(Map<String, Object> json) {
    return PlayerDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $PlayerDto = _$PlayerDtoTearOff();

/// @nodoc
mixin _$PlayerDto {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'pseudo')
  String get pseudo;
  @JsonKey(name: 'country')
  String get country;
  @JsonKey(name: 'age')
  int get age;
  @JsonKey(name: 'role')
  String get role;
  @JsonKey(name: 'jeu')
  String get jeu;
  @JsonKey(name: 'card_path')
  String get cardPath;
  @JsonKey(name: 'is_staff')
  bool get isStaff;
  @JsonKey(name: 'role_staff')
  String get roleStaff;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $PlayerDtoCopyWith<PlayerDto> get copyWith;
}

/// @nodoc
abstract class $PlayerDtoCopyWith<$Res> {
  factory $PlayerDtoCopyWith(PlayerDto value, $Res Function(PlayerDto) then) =
      _$PlayerDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'pseudo') String pseudo,
      @JsonKey(name: 'country') String country,
      @JsonKey(name: 'age') int age,
      @JsonKey(name: 'role') String role,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'card_path') String cardPath,
      @JsonKey(name: 'is_staff') bool isStaff,
      @JsonKey(name: 'role_staff') String roleStaff});
}

/// @nodoc
class _$PlayerDtoCopyWithImpl<$Res> implements $PlayerDtoCopyWith<$Res> {
  _$PlayerDtoCopyWithImpl(this._value, this._then);

  final PlayerDto _value;
  // ignore: unused_field
  final $Res Function(PlayerDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object pseudo = freezed,
    Object country = freezed,
    Object age = freezed,
    Object role = freezed,
    Object jeu = freezed,
    Object cardPath = freezed,
    Object isStaff = freezed,
    Object roleStaff = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      pseudo: pseudo == freezed ? _value.pseudo : pseudo as String,
      country: country == freezed ? _value.country : country as String,
      age: age == freezed ? _value.age : age as int,
      role: role == freezed ? _value.role : role as String,
      jeu: jeu == freezed ? _value.jeu : jeu as String,
      cardPath: cardPath == freezed ? _value.cardPath : cardPath as String,
      isStaff: isStaff == freezed ? _value.isStaff : isStaff as bool,
      roleStaff: roleStaff == freezed ? _value.roleStaff : roleStaff as String,
    ));
  }
}

/// @nodoc
abstract class _$PlayerDtoCopyWith<$Res> implements $PlayerDtoCopyWith<$Res> {
  factory _$PlayerDtoCopyWith(
          _PlayerDto value, $Res Function(_PlayerDto) then) =
      __$PlayerDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'pseudo') String pseudo,
      @JsonKey(name: 'country') String country,
      @JsonKey(name: 'age') int age,
      @JsonKey(name: 'role') String role,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'card_path') String cardPath,
      @JsonKey(name: 'is_staff') bool isStaff,
      @JsonKey(name: 'role_staff') String roleStaff});
}

/// @nodoc
class __$PlayerDtoCopyWithImpl<$Res> extends _$PlayerDtoCopyWithImpl<$Res>
    implements _$PlayerDtoCopyWith<$Res> {
  __$PlayerDtoCopyWithImpl(_PlayerDto _value, $Res Function(_PlayerDto) _then)
      : super(_value, (v) => _then(v as _PlayerDto));

  @override
  _PlayerDto get _value => super._value as _PlayerDto;

  @override
  $Res call({
    Object id = freezed,
    Object pseudo = freezed,
    Object country = freezed,
    Object age = freezed,
    Object role = freezed,
    Object jeu = freezed,
    Object cardPath = freezed,
    Object isStaff = freezed,
    Object roleStaff = freezed,
  }) {
    return _then(_PlayerDto(
      id: id == freezed ? _value.id : id as int,
      pseudo: pseudo == freezed ? _value.pseudo : pseudo as String,
      country: country == freezed ? _value.country : country as String,
      age: age == freezed ? _value.age : age as int,
      role: role == freezed ? _value.role : role as String,
      jeu: jeu == freezed ? _value.jeu : jeu as String,
      cardPath: cardPath == freezed ? _value.cardPath : cardPath as String,
      isStaff: isStaff == freezed ? _value.isStaff : isStaff as bool,
      roleStaff: roleStaff == freezed ? _value.roleStaff : roleStaff as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_PlayerDto implements _PlayerDto {
  const _$_PlayerDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'pseudo') this.pseudo,
      @JsonKey(name: 'country') this.country,
      @JsonKey(name: 'age') this.age,
      @JsonKey(name: 'role') this.role,
      @JsonKey(name: 'jeu') this.jeu,
      @JsonKey(name: 'card_path') this.cardPath,
      @JsonKey(name: 'is_staff') this.isStaff,
      @JsonKey(name: 'role_staff') this.roleStaff});

  factory _$_PlayerDto.fromJson(Map<String, dynamic> json) =>
      _$_$_PlayerDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'pseudo')
  final String pseudo;
  @override
  @JsonKey(name: 'country')
  final String country;
  @override
  @JsonKey(name: 'age')
  final int age;
  @override
  @JsonKey(name: 'role')
  final String role;
  @override
  @JsonKey(name: 'jeu')
  final String jeu;
  @override
  @JsonKey(name: 'card_path')
  final String cardPath;
  @override
  @JsonKey(name: 'is_staff')
  final bool isStaff;
  @override
  @JsonKey(name: 'role_staff')
  final String roleStaff;

  @override
  String toString() {
    return 'PlayerDto(id: $id, pseudo: $pseudo, country: $country, age: $age, role: $role, jeu: $jeu, cardPath: $cardPath, isStaff: $isStaff, roleStaff: $roleStaff)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PlayerDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.pseudo, pseudo) ||
                const DeepCollectionEquality().equals(other.pseudo, pseudo)) &&
            (identical(other.country, country) ||
                const DeepCollectionEquality()
                    .equals(other.country, country)) &&
            (identical(other.age, age) ||
                const DeepCollectionEquality().equals(other.age, age)) &&
            (identical(other.role, role) ||
                const DeepCollectionEquality().equals(other.role, role)) &&
            (identical(other.jeu, jeu) ||
                const DeepCollectionEquality().equals(other.jeu, jeu)) &&
            (identical(other.cardPath, cardPath) ||
                const DeepCollectionEquality()
                    .equals(other.cardPath, cardPath)) &&
            (identical(other.isStaff, isStaff) ||
                const DeepCollectionEquality()
                    .equals(other.isStaff, isStaff)) &&
            (identical(other.roleStaff, roleStaff) ||
                const DeepCollectionEquality()
                    .equals(other.roleStaff, roleStaff)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(pseudo) ^
      const DeepCollectionEquality().hash(country) ^
      const DeepCollectionEquality().hash(age) ^
      const DeepCollectionEquality().hash(role) ^
      const DeepCollectionEquality().hash(jeu) ^
      const DeepCollectionEquality().hash(cardPath) ^
      const DeepCollectionEquality().hash(isStaff) ^
      const DeepCollectionEquality().hash(roleStaff);

  @JsonKey(ignore: true)
  @override
  _$PlayerDtoCopyWith<_PlayerDto> get copyWith =>
      __$PlayerDtoCopyWithImpl<_PlayerDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PlayerDtoToJson(this);
  }
}

abstract class _PlayerDto implements PlayerDto {
  const factory _PlayerDto(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'pseudo') String pseudo,
      @JsonKey(name: 'country') String country,
      @JsonKey(name: 'age') int age,
      @JsonKey(name: 'role') String role,
      @JsonKey(name: 'jeu') String jeu,
      @JsonKey(name: 'card_path') String cardPath,
      @JsonKey(name: 'is_staff') bool isStaff,
      @JsonKey(name: 'role_staff') String roleStaff}) = _$_PlayerDto;

  factory _PlayerDto.fromJson(Map<String, dynamic> json) =
      _$_PlayerDto.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'pseudo')
  String get pseudo;
  @override
  @JsonKey(name: 'country')
  String get country;
  @override
  @JsonKey(name: 'age')
  int get age;
  @override
  @JsonKey(name: 'role')
  String get role;
  @override
  @JsonKey(name: 'jeu')
  String get jeu;
  @override
  @JsonKey(name: 'card_path')
  String get cardPath;
  @override
  @JsonKey(name: 'is_staff')
  bool get isStaff;
  @override
  @JsonKey(name: 'role_staff')
  String get roleStaff;
  @override
  @JsonKey(ignore: true)
  _$PlayerDtoCopyWith<_PlayerDto> get copyWith;
}
