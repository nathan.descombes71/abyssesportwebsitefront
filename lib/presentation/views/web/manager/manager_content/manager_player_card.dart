import 'package:abyssesportwebsite/domain/feature/players/entities/players.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/core/widgets/x_gradient_divider.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ManagerPlayerCard extends StatefulWidget {
  final Players actualPlayer;
  final RxBool canSeePlayers;
  final ManagerViewController controller;
  final int index;

  ManagerPlayerCard({
    Key key,
    this.actualPlayer,
    this.canSeePlayers,
    this.controller,
    this.index,
  }) : super(key: key);

  @override
  _ManagerPlayerCardState createState() => _ManagerPlayerCardState();
}

class _ManagerPlayerCardState extends State<ManagerPlayerCard> {
  RxDouble nameAnimation = (-40.0).obs;

  RxDouble containerAnim = (-70.0).obs;

  RxBool hoverring = false.obs;

  RxBool modifName = false.obs;
  RxBool modifRole = false.obs;
  RxBool modifAge = false.obs;
  RxBool modifCountry = false.obs;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Colors.transparent,
      color: Colors.transparent,
      elevation: 10,
      child: Obx(
        () => Container(
          color: Colors.transparent,
          height: 380,
          width: 250,
          child: Stack(
            children: [
              Container(
                height: 350,
                width: 250,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.fitHeight,
                        image: AssetImage('assets/graph/card_player.png')),
                    border: Border.all(color: Color(0xFFFD700FF), width: 3)),
                child: Container(
                  margin: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Stack(children: [
                    AnimatedPositioned(
                      onEnd: () {
                        if (hoverring.value == true) {
                          containerAnim.value = 50.0;
                        } else {
                          containerAnim.value = -70.0;
                        }
                      },
                      left: 10,
                      right: 10,
                      top:
                          !widget.canSeePlayers.value ? nameAnimation.value : 0,
                      duration: Duration(milliseconds: 100),
                      child: Obx(
                        () => Center(
                          child: !modifName.value
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: Container(
                                        width: 150,
                                        child: SelectableText(
                                          widget.actualPlayer.pseudo,
                                          style:
                                              kTextStyle.copyWith(fontSize: 30),
                                        ),
                                      ),
                                    ),
                                    IconButton(
                                        icon: Icon(
                                          MdiIcons.pencil,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          modifName.value = true;
                                        })
                                  ],
                                )
                              : Row(
                                  children: [
                                    Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        onChanged: (value) {
                                          widget.actualPlayer.pseudo = value;
                                        },
                                        decoration: InputDecoration(
                                            fillColor:
                                                Colors.grey.withOpacity(0.6),
                                            focusColor:
                                                Colors.grey.withOpacity(0.6),
                                            hoverColor:
                                                Colors.grey.withOpacity(0.6),
                                            border: InputBorder.none,
                                            hintText: ''),
                                        initialValue:
                                            widget.actualPlayer.pseudo,
                                      ),
                                    ),
                                    Expanded(
                                        child: InkWell(
                                      child: Icon(
                                        MdiIcons.check,
                                        color: Colors.white,
                                      ),
                                      onTap: () {
                                        widget.controller.savePlayer(
                                           widget.actualPlayer.id,
                                            widget.index,
                                            name: widget.actualPlayer.pseudo);
                                        modifName.value = false;
                                      },
                                    ))
                                  ],
                                ),
                        ),
                      ),
                    ),
                    AnimatedPositioned(
                        left: 10,
                        right: 10,
                        bottom: !widget.canSeePlayers.value
                            ? containerAnim.value
                            : 50,
                        duration: Duration(milliseconds: 200),
                        child: Center(
                          child: Obx(
                            () => Column(
                              children: [
                                !modifRole.value
                                    ? Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SelectableText(
                                              widget.actualPlayer.role),
                                          IconButton(
                                              icon: Icon(
                                                MdiIcons.pencil,
                                                color: Colors.white,
                                              ),
                                              onPressed: () {
                                                modifRole.value = true;
                                              })
                                        ],
                                      )
                                    : Row(
                                        children: [
                                          Expanded(
                                            flex: 5,
                                            child: TextFormField(
                                              onChanged: (value) {
                                                widget.actualPlayer.role =
                                                    value;
                                              },
                                              decoration: InputDecoration(
                                                  fillColor: Colors.grey
                                                      .withOpacity(0.6),
                                                  focusColor: Colors.grey
                                                      .withOpacity(0.6),
                                                  hoverColor: Colors.grey
                                                      .withOpacity(0.6),
                                                  border: InputBorder.none,
                                                  hintText: ''),
                                              initialValue:
                                                  widget.actualPlayer.role,
                                            ),
                                          ),
                                          Expanded(
                                              child: InkWell(
                                            child: Icon(
                                              MdiIcons.check,
                                              color: Colors.white,
                                            ),
                                            onTap: () {
                                              widget.controller.savePlayer(
                                                 widget.actualPlayer.id,
                                                  widget.index,
                                                  role:
                                                      widget.actualPlayer.role);
                                              modifRole.value = false;
                                            },
                                          ))
                                        ],
                                      ),
                                Container(
                                    margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child: XGradientDivider.white()),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    !modifCountry.value
                                        ? Row(
                                            children: [
                                              Image.asset(
                                                  'icons/flags/png/2.5x/' +
                                                      widget
                                                          .actualPlayer.country
                                                          .toLowerCase() +
                                                      '.png',
                                                  height: 20,
                                                  errorBuilder: (buildContext, object, stacktrace){
                                                    return SizedBox();
                                                  },
                                                  width: 20,
                                                  package: 'country_icons'),
                                              IconButton(
                                                  icon: Icon(
                                                    MdiIcons.pencil,
                                                    color: Colors.white,
                                                    size: 18,
                                                  ),
                                                  onPressed: () {
                                                    modifCountry.value = true;
                                                  })
                                            ],
                                          )
                                        : Container(
                                            height: 50,
                                            width: 70,
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 5,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      widget.actualPlayer
                                                          .country = value;
                                                    },
                                                    decoration: InputDecoration(
                                                        fillColor: Colors.grey
                                                            .withOpacity(0.6),
                                                        focusColor: Colors.grey
                                                            .withOpacity(0.6),
                                                        hoverColor: Colors.grey
                                                            .withOpacity(0.6),
                                                        border:
                                                            InputBorder.none,
                                                        hintText: ''),
                                                    initialValue: widget
                                                        .actualPlayer.country,
                                                  ),
                                                ),
                                                Expanded(
                                                    child: InkWell(
                                                  child: Icon(
                                                    MdiIcons.check,
                                                    color: Colors.white,
                                                  ),
                                                  onTap: () {
                                                    widget.controller
                                                        .savePlayer(
                                                          widget.actualPlayer.id,
                                                           widget.index,
                                                            country: widget
                                                                .actualPlayer
                                                                .country);
                                                    modifCountry.value = false;
                                                  },
                                                ))
                                              ],
                                            ),
                                          ),
                                    // Flag(
                                    //   actualPlayer.country.toLowerCase(),
                                    //   height: 20,
                                    //   width: 20,
                                    //   fit: BoxFit.fill,
                                    //   replacement: Text(actualPlayer.country),
                                    // ),
                                    SizedBox(
                                      width: 50,
                                    ),
                                    !modifAge.value
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SelectableText(widget
                                                  .actualPlayer.age
                                                  .toString()),
                                              InkWell(
                                                child: Icon(
                                                  MdiIcons.pencil,
                                                  color: Colors.white,
                                                  size: 18,
                                                ),
                                                onTap: () {
                                                  modifAge.value = true;
                                                },
                                              ),
                                            ],
                                          )
                                        : Container(
                                            height: 50,
                                            width: 70,
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 4,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      widget.actualPlayer.age =
                                                          int.parse(value);
                                                    },
                                                    decoration: InputDecoration(
                                                        fillColor: Colors.grey
                                                            .withOpacity(0.6),
                                                        focusColor: Colors.grey
                                                            .withOpacity(0.6),
                                                        hoverColor: Colors.grey
                                                            .withOpacity(0.6),
                                                        border:
                                                            InputBorder.none,
                                                        hintText: ''),
                                                    initialValue: widget
                                                        .actualPlayer.age
                                                        .toString(),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: InkWell(
                                                    
                                                    child: Icon(
                                                      MdiIcons.check,
                                                      color: Colors.white,
                                                    ),
                                                    onTap: () {
                                                      widget.controller
                                                          .savePlayer(
                                                             widget.actualPlayer.id,
                                                             widget.index,
                                                              age: widget
                                                                  .actualPlayer
                                                                  .age);
                                                      modifAge.value = false;
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        )),
                  ]),
                ),
              ),
              Positioned(
                left: 98,
                right: 98,
                bottom: 10,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.black,
                      border: Border.all(color: Color(0xFFFD700FF), width: 2),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: IconButton(
                      onPressed: () {
                        Get.dialog(dialogDeletePlayers(widget.actualPlayer));
                      },
                      icon: Icon(
                        MdiIcons.delete,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dialogDeletePlayers(Players players) {
    return Dialog(
        child: IntrinsicHeight(
      child: IntrinsicWidth(
        child: Container(
          color: Color(0xFFF262626),
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Text('Etes vous certains de vouloir supprimer ' +
                    players.pseudo),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Color(0xFF8338a1),
                        ),
                      ),
                      onPressed: () {
                        widget.controller
                            .deletePlayer(players.id, widget.index);
                        Get.back();
                      },
                      child: Text('Supprimer'),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('Annuler'),
                    ),
                  ],
                ),
              ],
            ),
          )),
        ),
      ),
    ));
  }
}
