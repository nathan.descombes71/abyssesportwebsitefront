import 'package:abyssesportwebsite/domain/feature/teams/controller/teams_controller.dart';
import 'package:abyssesportwebsite/domain/feature/teams/entities/teams.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/teams_repository_impl.dart';
import 'package:get/get.dart';

class TestViewController extends GetxController with StateMixin {
  TestViewController({this.teamRepo, this.teamsController});

  final TeamRepositoryImpl teamRepo;
  final TeamsController teamsController;

  RxList<Teams> listAllTeams = <Teams>[].obs;

  RxInt selectionnedItem = 0.obs;

  Rx<Teams> teamShow = Teams().obs;

  RxBool canSee = false.obs;

  @override
  void onInit() async {
    await teamsController.getTeamsList();
    // listAllTeams.assignAll(teamsController.allTeams);
    List<String> hashURL = [];
    hashURL = Get.currentRoute.split('/');

    if (hashURL.length == 3) {
      teamShow.value = listAllTeams.firstWhere(
          (element) => element.nameTeams.replaceAll(' ', '') == hashURL.last);
      selectionnedItem.value = listAllTeams.lastIndexWhere(
          (element) => element.nameTeams.replaceAll(' ', '') == hashURL.last);
    } else {
      teamShow.value = listAllTeams.first;
    }

    change(
      null,
      status: RxStatus.loading(),
    );
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
