import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'news_dto.freezed.dart';
part 'news_dto.g.dart';

@freezed
abstract class NewsDto with _$NewsDto {
  const factory NewsDto({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'author') String author,
    @JsonKey(name: 'link_image') String linkImage,
    @JsonKey(name: 'paragraph') List<String> paragraph,
    @JsonKey(name: 'created_at') String createdAt,
    @JsonKey(name: 'title') String title,
  }) = _NewsDto;
  factory NewsDto.fromJson(Map<String, dynamic> json) =>
      _$NewsDtoFromJson(json);
}

extension OnNewsJson on Map<String, dynamic> {
  NewsDto get toNewsDto {
    return NewsDto.fromJson(this);
  }
}

extension OnListNewsJson on List<Map<String, dynamic>> {
  List<NewsDto> get toNewsDto {
    return map<NewsDto>((Map<String, dynamic> e) => e.toNewsDto).toList();
  }
}
