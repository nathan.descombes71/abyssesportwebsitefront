import 'package:abyssesportwebsite/infrastructure/dtos/palmares_dto/palmares_dto.dart';
import 'package:abyssesportwebsite/infrastructure/dtos/player_dto/player_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'team_dto.freezed.dart';
part 'team_dto.g.dart';

@freezed
abstract class TeamDto with _$TeamDto {
  const factory TeamDto({
    @JsonKey(name: 'id') String id,
    @JsonKey(name: 'name_teams') String nameTeams,
    @JsonKey(name: 'path_logo_team') String pathLogoTeams,
    @JsonKey(name: 'jeu') String jeu,
    @JsonKey(name: 'list_players') List<PlayerDto> listPlayers,
    @JsonKey(name: 'palmares') List<PalmaresDto> listPalmares,
  }) = _TeamDto;
  factory TeamDto.fromJson(Map<String, dynamic> json) =>
      _$TeamDtoFromJson(json);
}

extension OnTeamJson on Map<String, dynamic> {
  TeamDto get toTeamDto {
    return TeamDto.fromJson(this);
  }
}

extension OnListTeamJson on List<Map<String, dynamic>> {
  List<TeamDto> get toTeamDto {
    return map<TeamDto>((Map<String, dynamic> e) => e.toTeamDto).toList();
  }
}
