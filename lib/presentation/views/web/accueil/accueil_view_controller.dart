import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/controller/sponsor_controller.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/entities/sponsor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/styles/styles.dart';

class AccueilViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {
  AccueilViewController({
    this.newsController,
    this.sponsorController,
  });

  final NewsController newsController;
  final SponsorController sponsorController;

  PageController pageController = PageController();

  RxDouble opawidget = 0.0.obs;

  Animation<double> myAnimation;
  AnimationController animationController;

  RxDouble sizeLogo = 55.0.obs;

  List lastNews = [];
  List allNews = [];

  List<Sponsor> sponsorList = <Sponsor>[];

  final RxInt currentIndex = 0.obs;

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;

  @override
  void onInit() async {
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );

    myAnimation =
        CurvedAnimation(curve: Curves.linear, parent: animationController);

    allNews = await newsController.getNewsList();
    lastNews.clear();

    debugPrint('la');

    if (allNews.length >= 1) {
      lastNews.add(allNews[allNews.length - 1]);
    }

    if (allNews.length >= 2) {
      lastNews.add(allNews[allNews.length - 2]);
    }

    if (allNews.length >= 3) {
      lastNews.add(allNews[allNews.length - 3]);
    }

    sponsorList = await sponsorController.getSponsorsList();
    change(
      null,
      status: RxStatus.loading(),
    );
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
    Future.delayed(Duration(milliseconds: 10))
        .then((value) => opawidget.value = 1.0);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
