import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/entities/sponsor.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/core/widgets/video_please.dart';

import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/scaffold/loading_scaffold.dart';
import 'package:abyssesportwebsite/presentation/views/web/contact/contact_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/contact/contact_view_controller_bindings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';
import 'accueil_content/accueil_cube_text.dart';
import 'accueil_view_controller.dart';

class AccueilView extends GetView<AccueilViewController> {
  AccueilView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: LoadingScaffold(),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: Column(
        children: [
          _upperView(),
          _middleView(),
          _lowerView(),
        ],
      ),
    );
  }

  Widget _upperView() {
    return Container(
      width: double.infinity,
      // height: 500,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/graph/dessusweb.png'),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 50, top: 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  height: 60,
                  width: 5,
                  color: Colors.white.withOpacity(0.3),
                ),
                SizedBox(width: 20),
                SelectableText(
                  "lastNews".tr,
                  style: kTitleStyle,
                ),
              ],
            ),
            Wrap(
              children: [
                for (var i = 0; i < controller.lastNews.length; i++)
                  Padding(
                    padding: EdgeInsets.only(
                      right: 40,
                      top: 30,
                    ),
                    child: Obx(
                      () => AnimatedOpacity(
                        opacity: controller.opawidget.value,
                        duration: Duration(seconds: 2),
                        child: lastNews(controller.lastNews[i]),
                      ),
                    ),
                  ),
              ],
            ),
            SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
    );
  }

  Widget lastNews(News lastNews) {
    RxDouble sizeContainer = 300.0.obs;
    Rx<Color> colorContainer = Colors.transparent.obs;
    RxBool visibleText = false.obs;
    return Container(
      height: 330,
      width: 330,
      child: Stack(
        children: [
          InkWell(
            onTap: () {
              Get.offAndToNamed(Routes.NEWS + '/' + lastNews.id.toString());
            },
            onHover: (isHovering) {
              if (isHovering) {
                visibleText.value = true;
                sizeContainer.value = 320;
                colorContainer.value = Colors.white.withOpacity(0.1);
              } else {
                visibleText.value = false;
                sizeContainer.value = 300;
                colorContainer.value = Colors.transparent;
              }
            },
            child: Obx(
              () => AnimatedContainer(
                duration: Duration(milliseconds: 100),
                height: sizeContainer.value,
                width: sizeContainer.value,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(lastNews.linkImage),
                  ),
                ),
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 500),
                  color: colorContainer.value,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 20),
                        child: Container(
                          child: Text(
                            lastNews.title,
                            style: kTitleStyle.copyWith(fontSize: 30),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _middleView() {
    return Container(
      color: Colors.transparent,
      child: LayoutBuilder(
        builder: (contextLayout, constraint) {
          return Padding(
            padding: EdgeInsets.only(top: 50, bottom: 50),
            child: Wrap(
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Container(
                  width: 800,
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 40,
                    ),
                    child: Center(
                      child: Container(
                        width: 500,
                        child: Column(
                          children: [
                            AccueilCubeText(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: Container(
                    child: VideoPlease(),
                    // decoration: BoxDecoration(
                    //     // color: Colors.black,
                    //     image: DecorationImage(
                    //         image: AssetImage('assets/graph/waiting.png'))),
                    height: contextLayout.width > 700 ? 400 : 200,
                    width: contextLayout.width > 700 ? 700 : 350,
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _lowerView() {
    return Container(
      // height: 500,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/graph/dessousweb.png'),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 60),
        child: Column(
          children: [
            SelectableText(
              'SPONSOR',
              style: TextStyle(
                fontSize: 50,
                fontFamily: kFontRaleway,
                color: Colors.white,
                // fontStyle: FontStyle.italic,
                // fontWeight: FontWeight.bold
              ),
            ),
            Text(
              'thank'.tr,
              style: TextStyle(
                fontSize: 20,
                fontFamily: kFontRaleway,
                color: Color(0xFFF80547b),
                fontStyle: FontStyle.italic,
                // fontWeight: FontWeight.bold
              ),
            ),
            Container(
              // height: 500,
              child: Center(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: controller.sponsorList.isNotEmpty
                      ? LayoutBuilder(
                          builder: (contextLayout, constraints) => Wrap(
                                alignment: WrapAlignment.center,
                                children: [
                                  for (var i = 0;
                                      i < controller.sponsorList.length;
                                      i++)
                                    sponsorLogo(
                                        sponsor: controller.sponsorList[i]),
                                ],
                              ))
                      : Padding(
                          padding: const EdgeInsets.fromLTRB(0, 50, 0, 50),
                          child: Text(
                            "anySponsor".tr,
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                ),
              ),
            ),
            ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                )),
                backgroundColor: MaterialStateProperty.all<Color>(
                  Color(0xFF8338a1),
                ),
              ),
              onPressed: () {
                Get.off(
                  ContactView(),
                  arguments: {
                    'isSponsoring': true,
                  },
                  binding: ContactViewControllerBindings(),
                );
              },
              child: Text('becameSponsor'.tr),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              height: 5,
              width: double.infinity,
              color: Color(0xFFFD700FF),
            ),
            _footer(),
          ],
        ),
      ),
    );
  }

  Widget sponsorLogo({Sponsor sponsor}) {
    RxDouble sizeLogoSponsor = 150.0.obs;
    return Container(
      height: 200,
      width: 200,
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Center(
        child: InkWell(
          onTap: () {
            _launchURL(sponsor.linkRedirect);
          },
          onHover: (isHover) {
            if (isHover) {
              sizeLogoSponsor.value = 180;
            } else {
              sizeLogoSponsor.value = 150;
            }
            print(sizeLogoSponsor.value);
          },
          child: Obx(
            () => AnimatedContainer(
              height: sizeLogoSponsor.value,
              width: sizeLogoSponsor.value,
              duration: Duration(milliseconds: 200),
              child: Image.network(
                sponsor.pathImage,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _footer() {
    return Container(
      // height: 60,¤
      width: double.infinity,
      // color: Colors.grey,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(30, 30, 30, 30),
        child: Wrap(
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                'Legals Mentions',
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: 14,
                    color: Colors.white.withOpacity(0.5)),
              ),
              SizedBox(width: 50, height: 50),
              Text(
                '©  2021 AbyssDeveloppment ' + 'allRightsReserved'.tr,
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: 14,
                    color: Colors.white.withOpacity(0.5)),
              )
            ]),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
