import 'package:flutter/foundation.dart';

abstract class ControllerBase<R> {
  final R repository;
  ControllerBase({
    @required this.repository,
  });
}
