import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/controller/sponsor_controller.dart';
import 'package:abyssesportwebsite/domain/feature/teams/controller/teams_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'manager_view_controller.dart';

class ManagerViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ManagerViewController(
        Get.put<TeamsController>(
          TeamsController(),
        ),
        Get.put<SponsorController>(
          SponsorController(),
        ),
        Get.put<NewsController>(
          NewsController(),
        ),
      ),
    );
  }
}
