import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'player_dto.freezed.dart';
part 'player_dto.g.dart';

@freezed
abstract class PlayerDto with _$PlayerDto {
  const factory PlayerDto({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'pseudo') String pseudo,
    @JsonKey(name: 'country') String country,
    @JsonKey(name: 'age') int age,
    @JsonKey(name: 'role') String role,
    @JsonKey(name: 'jeu') String jeu,
    @JsonKey(name: 'card_path') String cardPath,
    @JsonKey(name: 'is_staff') bool isStaff,
    @JsonKey(name: 'role_staff') String roleStaff,
  }) = _PlayerDto;
  factory PlayerDto.fromJson(Map<String, dynamic> json) =>
      _$PlayerDtoFromJson(json);
}

extension OnPlayerJson on Map<String, dynamic> {
  PlayerDto get toPlayerDto {
    return PlayerDto.fromJson(this);
  }
}

extension OnListPlayerJson on List<Map<String, dynamic>> {
  List<PlayerDto> get toPlayerDto {
    return map<PlayerDto>((Map<String, dynamic> e) => e.toPlayerDto).toList();
  }
}
