import 'package:abyssesportwebsite/infrastructure/dtos/palmares_dto/palmares_dto.dart';
import 'package:flutter/material.dart';

class User {
  String email;
  String pseudo;
  bool isManager;

  User({this.email, this.pseudo, this.isManager});
}

extension OnUser on User {
  User copyWith({
    String email,
    String pseudo,
    bool isManager,
  }) {
    return User(
      email: email ?? this.email,
      pseudo: pseudo ?? this.pseudo,
      isManager: isManager ?? this.isManager,
    );
  }

  //   UserDto get toDto {
  //   return PalmaresDto(
  //     email: email,
  //     email: email,
  //     );
  // }
}

// extension OnListPalmares on List<Palmares> {
//   List<PalmaresDto> get toDto {
//     List<PalmaresDto> _palmaresDtoList = [];

//     this?.forEach((entity) => _palmaresDtoList.add(entity.toDto));
//     return _palmaresDtoList;
//   }
// }

// extension OnListPalmaresDto on List<PalmaresDto> {
//   List<Palmares> get toEntity {
//     List<Palmares> _palmaresList = [];

//     this?.forEach((dto) => _palmaresList.add(dto.toEntity));
//     return _palmaresList;
//   }
// }

// extension OnPalmaresDto on PalmaresDto {
//   Palmares get toEntity {
//     return Palmares(
//       id: id,
//       nameCup: nameCup,
//       position: position,
//     );
//   }
// }
