import 'package:abyssesportwebsite/domain/feature/players/entities/players.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/core/widgets/x_gradient_divider.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TeamsPlayerCard extends StatelessWidget {
  final Players actualPlayer;
  final RxBool canSeePlayers;

  TeamsPlayerCard({Key key, this.actualPlayer, this.canSeePlayers})
      : super(key: key);

  RxDouble nameAnimation = (-40.0).obs;
  RxDouble containerAnim = (-70.0).obs;
  RxBool hoverring = false.obs;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: InkWell(
        onTap: () {
          // Get.toNamed(Routes.TEAMS + '/' + actualPlayer.pseudo);
        },
        onHover: (isHovering) {
          if (isHovering) {
            hoverring.value = true;
            nameAnimation.value = 0.0;
          } else {
            hoverring.value = false;
            nameAnimation.value = -40.0;
            containerAnim.value = -70.0;
          }
        },
        child: Obx(
          () => Container(
            height: 350,
            width: 250,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fitHeight,
                    image: AssetImage('assets/graph/card_player.png')),
                border: Border.all(color: Color(0xFFFD700FF), width: 3)),
            child: Container(
              margin: EdgeInsets.only(top: 10),
              child: Stack(children: [
                AnimatedPositioned(
                  onEnd: () {
                    if (hoverring.value == true) {
                      containerAnim.value = 50.0;
                    } else {
                      containerAnim.value = -70.0;
                    }
                  },
                  left: 10,
                  right: 10,
                  top: !canSeePlayers.value ? nameAnimation.value : 0,
                  duration: Duration(milliseconds: 100),
                  child: Center(
                    child: SelectableText(
                      actualPlayer.pseudo,
                      style: kTextStyle.copyWith(fontSize: 30),
                    ),
                  ),
                ),
                AnimatedPositioned(
                  left: 10,
                  right: 10,
                  bottom: !canSeePlayers.value ? containerAnim.value : 50,
                  duration: Duration(milliseconds: 200),
                  child: Center(
                    child: Column(
                      children: [
                        SelectableText(actualPlayer.role),
                        Container(
                            margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: XGradientDivider.white()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                                'icons/flags/png/2.5x/' +
                                    actualPlayer.country.toLowerCase() +
                                    '.png',
                                height: 20,
                                width: 20,
                                package: 'country_icons'),
                            // Flag(
                            //   actualPlayer.country.toLowerCase(),
                            //   height: 20,
                            //   width: 20,
                            //   fit: BoxFit.fill,
                            //   replacement: Text(actualPlayer.country),
                            // ),
                            SizedBox(
                              width: 50,
                            ),
                            SelectableText(actualPlayer.age.toString()),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
