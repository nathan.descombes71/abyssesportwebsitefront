import 'package:abyssesportwebsite/presentation/core/helpers/data_converter.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/core/widgets/x_gradient_divider.dart';
import 'package:abyssesportwebsite/presentation/scaffold/loading_scaffold.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'details_niews_view_controller.dart';

class DetailsNiewsView extends GetView<DetailsNiewsViewController> {
  DetailsNiewsView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onLoading: LoadingScaffold(),
    );
  }

  ScrollController scrollController = ScrollController();

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      body: Center(
        child: LayoutBuilder(
          builder: (contextLayout, constraint) => Container(
            margin: EdgeInsets.only(
                left:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0,
                right:
                    contextLayout.width > 800 ? contextLayout.width * 0.15 : 0),
            padding: EdgeInsets.fromLTRB(100, 60, 100, 60),
            color: Color(0xFFF262626),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(40, 0, 0, 50),
                  child: Row(
                    children: [
                      // if (contextLayout.width > 1000)
                      Container(
                        height: 60,
                        width: 5,
                        color: Colors.white.withOpacity(0.3),
                      ),
                      // if (contextLayout.width > 1000)
                      SizedBox(width: 20),
                      Container(
                        width: contextLayout.width * 0.55,
                        child: SelectableText(
                          controller.actualNews.value.title,
                          style: kTitleStyle,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      if (FirebaseAuth.instance.currentUser != null)
                        IconButton(
                            onPressed: () {
                              controller.deleteNews();
                            },
                            icon: Icon(
                              MdiIcons.delete,
                              color: Colors.red,
                              size: 30,
                            ))
                    ],
                  ),
                ),
                Center(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(maxHeight: 500),
                    child: Image.network(
                      controller.actualNews.value.linkImage,
                      // height: 300,
                      // width: 300,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 75, bottom: 25),
                  child: XGradientDivider.left(),
                ),
                // Container(
                //   color: Colors.white.withOpacity(0.3),
                //   margin: EdgeInsets.fromLTRB(200, 50, 200, 50),
                // ),
                for (var i = 0;
                    i < controller.actualNews.value.paragraph.length;
                    i++)
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                    ),
                    child: SelectableText.rich(
                      TextSpan(
                        children: controller.allParagraph[i],
                      ),
                    ),
                  ),
                Container(
                  margin: EdgeInsets.only(top: 75, bottom: 75),
                  child: XGradientDivider.left(),
                ),
                SelectableText(
                    'writtenBy'.tr + ' ' + controller.actualNews.value.author),

                SelectableText(
                    'on'.tr + ' ' + controller.actualNews.value.createdAt),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
