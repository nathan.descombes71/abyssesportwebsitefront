import 'package:abyssesportwebsite/domain/auth/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreateArticleViewController extends GetxController with StateMixin {
  CreateArticleViewController();

  String email = '';
  String pass = '';
  RxString errorMessage = ''.obs;

  List<String> listParagraph = [''];

  @override
  void onInit() {
    change(
      null,
      status: RxStatus.loading(),
    );

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
