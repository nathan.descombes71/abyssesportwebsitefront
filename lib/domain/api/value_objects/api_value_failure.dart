import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_value_failure.freezed.dart';

@freezed
abstract class ApiValueFailure<T> with _$ApiValueFailure<T> {
  const factory ApiValueFailure.notFound({
    @required String message,
  }) = NotFound<T>;
  const factory ApiValueFailure.jwtExpired({
    @required String message,
  }) = JwtExpired<T>;
  const factory ApiValueFailure.serverError({
    @required String message,
  }) = ServerError<T>;
  const factory ApiValueFailure.serializationError({
    @required String message,
  }) = SerializationError<T>;
  const factory ApiValueFailure.empty({
    @required String message,
  }) = Empty<T>;
}
