// import 'dart:io';

import 'dart:html';

import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
import 'package:abyssesportwebsite/domain/feature/players/entities/players.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/controller/sponsor_controller.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/entities/sponsor.dart';
import 'package:abyssesportwebsite/domain/feature/teams/controller/teams_controller.dart';
import 'package:abyssesportwebsite/domain/feature/teams/entities/palmares.dart';
import 'package:abyssesportwebsite/domain/feature/teams/entities/teams.dart';
import 'package:abyssesportwebsite/presentation/core/extensions/date_factory.dart';
import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/views/web/news/news_content/news_card.dart';
import 'package:firebase_storage/firebase_storage.dart' as fireBaseStorage;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ManagerViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {
  ManagerViewController(
      this.teamsController, this.sponsorController, this.newsController);
  final TeamsController teamsController;
  final SponsorController sponsorController;
  final NewsController newsController;

  RxList<Rx<ImageChoose>> listAllImagePicked = <Rx<ImageChoose>>[].obs;

  // Rx<File> imagee = File('').obs;

  List<String> tabs = [
    'Equipes',
    'Article',
    'Sponsor',
  ];

  RxString errorMessage = ''.obs;

  TabController tabController;

  ScrollController controllerScroll = ScrollController();

  RxList<Sponsor> sponsorList = <Sponsor>[].obs;

  RxList<Teams> teamsList = <Teams>[].obs;

  ScrollController scrollbar = ScrollController();
  RxBool constructDialog = false.obs;

  String nameTeams = '';
  String nameGames = '';

  String nameCup = '';
  String posTeam = '';

  @override
  void onInit() async {
    change(
      null,
      status: RxStatus.loading(),
    );

    tabController = TabController(length: 3, vsync: this, initialIndex: 0);

    teamsList.assignAll(await teamsController.getTeamsList());

    teamsList.forEach((element) {
      listAllImagePicked
          .add(ImageChoose(linkImage: null, idTeams: element.id).obs);
    });

    sponsorList.assignAll(await sponsorController.getSponsorsList());

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void deletePalmares(int id, int indexTeams) {
    teamsList[indexTeams]
        .listPalmares
        .removeWhere((element) => element.id == id);
    teamsController.updateTeams(
      teamsList[indexTeams],
    );
    constructDialog.value = !constructDialog.value;
    refresh();
  }

  void addPalmares(int indexTeam) async {
    int id = 0;

    try {
      if (teamsList[indexTeam].listPalmares.isNotEmpty) {
        id = teamsList[indexTeam].listPalmares.last.id + 1;
      }
      teamsList[indexTeam].listPalmares.add(Palmares(
            id: id,
            nameCup: nameCup,
            position: int.parse(posTeam),
          ));
      await teamsController.updateTeams(teamsList[indexTeam]);
      constructDialog.value = !constructDialog.value;
      refresh();
    } catch (e) {
      throw e;
    }
  }

  void savePlayer(int idPlayer, int index,
      {String name, String role, int age, String country}) async {
    int indexPlayer = teamsList[index]
        .listPlayers
        .lastIndexWhere((element) => element.id == idPlayer);

    if (name != null) {
      teamsList[index].listPlayers[indexPlayer].pseudo = name;
    } else if (role != null) {
      teamsList[index].listPlayers[indexPlayer].role = role;
    } else if (age != null) {
      teamsList[index].listPlayers[indexPlayer].age = age;
    } else if (country != null) {
      teamsList[index].listPlayers[indexPlayer].country = country;
    }

    teamsController.updateTeams(teamsList[index]);
  }

  void deletePlayer(int id, int index) {
    teamsList[index].listPlayers.removeWhere((element) => element.id == id);
    teamsController.updateTeams(
      teamsList[index],
    );
    refresh();
  }

  void deleteTeams(String idTeam) {
    try {
      teamsController.deleteTeams(idTeam);
      teamsList.removeWhere((element) => element.id == idTeam);

      listAllImagePicked
          .removeWhere((element) => element.value.idTeams == idTeam);

      refresh();
    } catch (e) {
      throw (e);
    }
  }

  var selectedImagePath = ''.obs;
  // var selectedImageSize = ''.obs;

  Future getImage(String idTeams) async {
    try {
      ImagePicker picker = ImagePicker();
      final pickedFile = await picker.getImage(source: ImageSource.gallery);

      int indexOf = listAllImagePicked
          .lastIndexWhere((element) => element.value.idTeams == idTeams);

      if (pickedFile != null) {
        listAllImagePicked[indexOf].value.linkImage = pickedFile;
      }

      refresh();
    } catch (e) {
      debugPrint(e);
      throw (e);
    }
  }

  void createTeam() async {
    try {
      var string = await teamsController.createTeams(
        Teams(
          id: nameTeams.replaceAll(' ', ''),
          jeu: nameGames,
          nameTeams: nameTeams,
          listPlayers: [],
          listPalmares: [],
        ),
      );

      print(string);

      listAllImagePicked.add(
          ImageChoose(linkImage: null, idTeams: nameTeams.replaceAll(' ', ''))
              .obs);

      print(listAllImagePicked);

      teamsList.add(
        Teams(
            id: nameTeams.replaceAll(' ', ''),
            jeu: nameGames,
            nameTeams: nameTeams,
            listPlayers: [],
            listPalmares: []),
      );

      Get.back();
      nameGames = '';
      nameTeams = '';
    } catch (e) {
      throw (e);
    }
  }

  void createPlayer(int index) {
    int id = 0;

    if (teamsList[index].listPlayers.isNotEmpty) {
      id = teamsList[index].listPlayers.last.id + 1;
    }
    teamsList[index].listPlayers.add(Players(
        id: id,
        age: 1,
        country: 'FR',
        isStaff: false,
        jeu: teamsList[index].jeu,
        pseudo: '',
        role: ''));

    teamsController.updateTeams(teamsList[index]);
    refresh();
  }

  void sendImage(String idTeams) async {
    try {
      fireBaseStorage.Reference ref =
          fireBaseStorage.FirebaseStorage.instance.ref();

      int indexOf = listAllImagePicked
          .lastIndexWhere((element) => element.value.idTeams == idTeams);

      // var file = File(listAllImagePicked[indexOf].value.linkImage);

      final metadata = fireBaseStorage.SettableMetadata(
          contentType: 'image/png',
          customMetadata: {
            'picked-file-path': listAllImagePicked[indexOf].value.linkImage.path
          });

      print('ici');
      var snapshot = await ref.child('teams/$idTeams').putData(
          await listAllImagePicked[indexOf].value.linkImage.readAsBytes(),
          metadata);

      var downloadUrl = snapshot.ref.getDownloadURL();

      Teams changeTeams = teamsList
          .lastWhere((element) => element.id == idTeams)
          .copyWith(pathLogoTeam: await downloadUrl);

      teamsController.updateTeams(changeTeams);

      succesSnackBar(text: 'Image envoyé avec succès !');
    } catch (e) {
      throw e;
    }
  }

  //ARTICLE VIEWS

  String author = '';
  String titleNews = '';
  Rx<PickedFile> imageArticle = PickedFile('').obs;
  List<String> listParagraph = [''];

  Future getImageArticle() async {
    try {
      ImagePicker picker = ImagePicker();
      final pickedFile = await picker.getImage(source: ImageSource.gallery);

      if (pickedFile != null) {
        print(pickedFile.path);
        imageArticle.value = pickedFile;
      }

      refresh();
    } catch (e) {
      debugPrint(e);
      throw (e);
    }
  }

  void createArticle() async {
    int idArticle = int.parse(
        "${DateTime.now().day}${DateTime.now().month}${DateTime.now().year}${DateTime.now().hour}${DateTime.now().minute}${DateTime.now().second}");

    fireBaseStorage.Reference ref =
        fireBaseStorage.FirebaseStorage.instance.ref();

    final metadata = fireBaseStorage.SettableMetadata(
        contentType: 'image/png',
        customMetadata: {'picked-file-path': imageArticle.value.path});

    var snapshot = await ref
        .child('news/$idArticle')
        .putData(await imageArticle.value.readAsBytes(), metadata);

    var downloadUrl = await snapshot.ref.getDownloadURL();

    News newsSend = News(
        author: author,
        createdAt: DateFactory().dateToString(DateTime(2021, 10, 10)),
        id: idArticle,
        title: titleNews,
        paragraph: listParagraph,
        linkImage: downloadUrl);

    await newsController.createNew(newsSend);

    succesSnackBar(text: "News créé avec succès");
  }

  // SPONSOR VIEWS

  Rx<PickedFile> logoSponsor = PickedFile('').obs;
  String nameSponsor = '';
  String linkRedirectSponsor = '';

  Future getLogoSponsor() async {
    try {
      ImagePicker picker = ImagePicker();
      final pickedFile = await picker.getImage(source: ImageSource.gallery);

      if (pickedFile != null) {
        print(pickedFile.path);
        logoSponsor.value = pickedFile;
      }

      refresh();
    } catch (e) {
      debugPrint(e);
      throw (e);
    }
  }

  void createSponsor() async {
    try {
      int idSponsor = 1;
      if (sponsorList.isNotEmpty) {
        idSponsor = sponsorList.last.id + 1;
      }

      fireBaseStorage.Reference ref =
          fireBaseStorage.FirebaseStorage.instance.ref();

      final metadata = fireBaseStorage.SettableMetadata(
          contentType: 'image/png',
          customMetadata: {'picked-file-path': logoSponsor.value.path});

      var snapshot = await ref
          .child('logoSponsor/SponsorLogo$idSponsor')
          .putData(await logoSponsor.value.readAsBytes(), metadata);

      var downloadUrl = await snapshot.ref.getDownloadURL();

      await sponsorController.createSponsor(
        Sponsor(
            id: idSponsor,
            linkRedirect: linkRedirectSponsor,
            nameSponsor: nameSponsor,
            pathImage: downloadUrl),
      );
      sponsorList.add(
        Sponsor(
            id: idSponsor,
            linkRedirect: linkRedirectSponsor,
            nameSponsor: nameSponsor,
            pathImage: downloadUrl),
      );

      refresh();
    } catch (e) {
      throw (e);
    }
  }

  void deleteSponsor(int idSponsor) {
    sponsorController.deleteSponsor(idSponsor);
    sponsorList.removeWhere((element) => element.id == idSponsor);
    refresh();
  }
}

class ImageChoose {
  PickedFile linkImage;
  String idTeams;

  ImageChoose({this.linkImage, this.idTeams});
}
