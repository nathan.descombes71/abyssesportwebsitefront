import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AccueilCubeText extends StatelessWidget {
  ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                color: Colors.white.withOpacity(0.3),
                height: 18,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: SelectableText(
                'about'.tr,
                style: kTitleStyle,
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.white.withOpacity(0.3),
                height: 18,
              ),
            ),
          ],
        ),
        Container(
          height: 250,
          child: Row(
            children: [
              Container(
                transform: Matrix4.translationValues(0.0, -14.6, 0.0),
                color: Colors.white.withOpacity(0.3),
                // height: double.infinity,
                width: 18,
              ),
              Container(
                // color: Colors.white.withOpacity(0.3),
                width: 464,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: 30, bottom: 44.6, left: 30, right: 30),
                  child: Scrollbar(
                    isAlwaysShown: true,
                    controller: scrollController,
                    child: SingleChildScrollView(
                      controller: scrollController,
                      child: SelectableText("aboutusText".tr),
                    ),
                  ),
                ),
              ),
              Container(
                transform: Matrix4.translationValues(0.0, -14.6, 0.0),
                color: Colors.white.withOpacity(0.3),
                // height: 300,
                width: 18,
              ),
            ],
          ),
        ),
        Container(
          transform: Matrix4.translationValues(0.0, -14.6, 0.0),
          color: Colors.white.withOpacity(0.3),
          height: 18,
          // width: 18,
        )
      ],
    ));
  }
}
