import 'package:abyssesportwebsite/infrastructure/dtos/sponsor_dto/sponsor_dto.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/rest_api_repository_factory.dart';

abstract class SponsorRepository extends RestApiRepositoryFactory<SponsorDto> {
  Future<List<SponsorDto>> getSponsorDtoList({
    Map<String, dynamic> queryParameters,
  });
  Future<String> createSponsor(SponsorDto sponsor);
  Future<String> deleteSponsor(int idSponsor);
}
