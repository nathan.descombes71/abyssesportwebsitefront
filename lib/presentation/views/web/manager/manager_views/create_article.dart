import 'package:abyssesportwebsite/presentation/core/styles/styles.dart';
import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:abyssesportwebsite/presentation/views/web/manager/manager_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CreateArticle extends GetView<ManagerViewController> {
  CreateArticle({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildContent(context);
  }

  Widget buildContent(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            children: [
              Container(
                height: 60,
                width: 5,
                color: Colors.white.withOpacity(0.3),
              ),
              SizedBox(width: 20),
              SelectableText(
                "CREER UN ARTICLE",
                style: kTitleStyle,
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            width: 300,
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              // obscureText: true,
              onChanged: (value) {
                controller.titleNews = value;
              },
              decoration: InputDecoration(
                labelText: "Titre",
                // fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                  borderSide: new BorderSide(),
                ),
              ),
              // initialValue: 'test',
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: 250,
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              // obscureText: true,
              onChanged: (value) {
                controller.author = value;
              },
              decoration: InputDecoration(
                labelText: "Nom de l'auteur",
                // fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                  borderSide: new BorderSide(),
                ),
              ),
              // initialValue: 'test',
            ),
          ),
          SizedBox(
            height: 30,
          ),
          controller.imageArticle.value.path == ''
              ? SizedBox()
              : Image.network(
                  controller.imageArticle.value.path,
                  width: 500,
                  height: 500,
                ),
          SizedBox(
            height: 30,
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(
                Color(0xFF8338a1),
              ),
            ),
            onPressed: () {
              controller.getImageArticle();
            },
            child: Text('Ajouter une photo'),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            // height: 1000,
            width: 600,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: controller.listParagraph.length,
                itemBuilder: (context, index) {
                  return Container(
                    height: 150,
                    width: 500,
                    child: Row(
                      children: [
                        Container(
                          height: 150,
                          width: 450,
                          child: TextFormField(
                            controller: TextEditingController(
                                text: controller.listParagraph[index]),
                            maxLines: 4,
                            style: TextStyle(color: Colors.white),
                            // obscureText: true,
                            onChanged: (value) {
                              controller.listParagraph[index] = value;
                            },
                            decoration: InputDecoration(
                              hintText: "Paragraphe " + (index + 1).toString(),
                              // fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            // initialValue: 'test',
                          ),
                        ),
                        Center(
                          child: index != 0
                              ? IconButton(
                                  icon:
                                      Icon(MdiIcons.delete, color: Colors.red),
                                  onPressed: () {
                                    controller.listParagraph.removeAt(index);
                                    controller.refresh();
                                    print(controller.listParagraph);
                                  })
                              : SizedBox(),
                        )
                      ],
                    ),
                  );
                }),
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(
                Color(0xFF8338a1),
              ),
            ),
            onPressed: () {
              controller.listParagraph.add('');
              controller.refresh();
            },
            child: Text('Ajouter un paragraphe'),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(
                Color(0xFF8338a1),
              ),
            ),
            onPressed: () {
              controller.createArticle();
            },
            child: Text('Envoyer l\'article'),
          ),
          SizedBox(
            height: 100,
          )
        ],
      ),
    );
  }
}
