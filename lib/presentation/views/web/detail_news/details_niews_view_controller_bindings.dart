import 'package:abyssesportwebsite/domain/feature/news/controller/news_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'details_niews_view_controller.dart';

class DetailsNiewsViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => DetailsNiewsViewController(),
    );
  }
}
