import 'package:abyssesportwebsite/presentation/navigation/routes.dart';
import 'package:abyssesportwebsite/presentation/scaffold/scaffold_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/scheduler/ticker.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'dart:js' as js;

import '../core/styles/styles.dart';
import 'scaffold_content/scaffold_triangle.dart';

class LoadingScaffold extends StatefulWidget {
  final Widget body;
  LoadingScaffold({Key key, this.body}) : super(key: key);

  @override
  _LoadingScaffoldState createState() => _LoadingScaffoldState();
}

class _LoadingScaffoldState extends State<LoadingScaffold>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return buildContent(context);
  }

// Image.asset('assets/graph/banniere2.png'),
  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isCharged: false,
      body: ConstrainedBox(
        constraints: BoxConstraints(minHeight: context.height - 105),
        child: Container(
          color: Color(0xFFF262626),
          child: Column(
            children: [
              LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Color(0xFF7000a3),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // void _launchURL(String url) async =>
  //     await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
