import 'package:abyssesportwebsite/domain/feature/sponsor/entities/sponsor.dart';
import 'package:abyssesportwebsite/domain/feature/sponsor/repositories/sponsor_repositories.dart';
import 'package:abyssesportwebsite/infrastructure/repositories/sponsor_repository_impl.dart';

class SponsorController {
  SponsorController();

  List<Sponsor> allSponsor = [
    Sponsor(
      id: 1,
      linkRedirect: 'https://www.carrefour.fr/',
      nameSponsor: 'Carrefour',
      pathImage:
          'https://firebasestorage.googleapis.com/v0/b/abyss-esport.appspot.com/o/logoSponsor%2FCarrefour-Logo-1966-1972.png?alt=media&token=bc3db3b8-ce3b-404c-ab97-762c5eb34261',
    ),
    Sponsor(
      id: 1,
      linkRedirect: 'https://www.carrefour.fr/',
      nameSponsor: 'Carrefour',
      pathImage:
          'https://firebasestorage.googleapis.com/v0/b/abyss-esport.appspot.com/o/logoSponsor%2FCarrefour-Logo-1966-1972.png?alt=media&token=bc3db3b8-ce3b-404c-ab97-762c5eb34261',
    ),
    Sponsor(
      id: 1,
      linkRedirect: 'https://www.carrefour.fr/',
      nameSponsor: 'Carrefour',
      pathImage:
          'https://firebasestorage.googleapis.com/v0/b/abyss-esport.appspot.com/o/logoSponsor%2FCarrefour-Logo-1966-1972.png?alt=media&token=bc3db3b8-ce3b-404c-ab97-762c5eb34261',
    ),
    Sponsor(
      id: 1,
      linkRedirect: 'https://www.carrefour.fr/',
      nameSponsor: 'Carrefour',
      pathImage:
          'https://firebasestorage.googleapis.com/v0/b/abyss-esport.appspot.com/o/logoSponsor%2FCarrefour-Logo-1966-1972.png?alt=media&token=bc3db3b8-ce3b-404c-ab97-762c5eb34261',
    ),
  ];

  SponsorRepository sponsorRepository = SponsorRepositoryImpl();

  Future<List<Sponsor>> getSponsorsList() async {
    List<Sponsor> sponsor = [];

    sponsor = await sponsorRepository
        .getSponsorDtoList()
        .then((value) => value.toEntity);
    print("ici");
    return sponsor;
  }

  Future<String> createSponsor(Sponsor sponsor) async {
    return await sponsorRepository.createSponsor(sponsor.toDto);
  }

  Future<String> deleteSponsor(int sponsorId) async {
    return await sponsorRepository.deleteSponsor(sponsorId);
  }
}
